package antlr.collections.impl;


/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ASTEnumerator.java,v 1.1 2004/03/13 10:34:47 elinares Exp $
 */
import antlr.collections.AST;

public class ASTEnumerator implements antlr.collections.ASTEnumeration {
   /** The list of root nodes for subtrees that match */
   VectorEnumerator nodes;
   int              i=0;

   public ASTEnumerator(Vector v) {
      nodes=new VectorEnumerator(v);
   }

   public boolean hasMoreNodes() {
      synchronized(nodes) {
         return i<=nodes.vector.lastElement;
      }
   }

   public antlr.collections.AST nextNode() {
      if(i<=nodes.vector.lastElement) {
         return (AST)nodes.vector.data[i++];
      }

      return null;
   }
}
