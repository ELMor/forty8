package f8.op.prg.brch;

import antlr.collections.AST;
import f8.Command;
import f8.NonAlgebraic;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class IF extends NonAlgebraic {
   Command cond;
   Command thenBody;
   Command elseBody;

   public IF(AST def) {
      if(def==null) {
         return;
      }

      AST aCond=def.getFirstChild();
      AST aThen=aCond.getNextSibling();
      AST aElse=aThen.getNextSibling();
      cond       =Command.createFromAST(aCond);
      thenBody   =Command.createFromAST(aThen);

      if(def.getType()==ObjectParserTokenTypes.IFELSE) {
         elseBody=Command.createFromAST(aElse);
      }
   }

   public int getID() {
      return 23;
   }

   public Storable getInstance() {
      return new IF(null);
   }

   public void loadState(DataStream ds) {
      cond       =Command.loadFromStorage(ds);
      thenBody   =Command.loadFromStorage(ds);

      int tipo   =ds.readInt();

      if(tipo==1) {
         elseBody=Command.loadFromStorage(ds);
      } else {
         elseBody=null;
      }
   }

   public void saveState(DataStream ds) {
      ds.writeInt(cond.getID());
      cond.saveState(ds);

      ds.writeInt(thenBody.getID());
      thenBody.saveState(ds);

      ds.writeInt((elseBody==null) ? 0 : 1);

      if(elseBody!=null) {
         ds.writeInt(elseBody.getID());
         elseBody.saveState(ds);
      }
   }

   public void exec() throws F8Exception {
      cond.exec();

      int res=UtilHelper.evalCondition();

      if(res==1) {
         thenBody.exec();
      }

      if((res==0)&&(elseBody!=null)) {
         elseBody.exec();
      }
   }

   public String toString() {
      return "IF "+cond.toString()+" THEN "+thenBody.toString()
             +((elseBody==null) ? "" : (" ELSE "+elseBody.toString()))+" END";
   }
}
