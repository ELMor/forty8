package f8.op.prg.brch;

import antlr.collections.AST;
import f8.Command;
import f8.NonAlgebraic;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class IFERR extends NonAlgebraic {
   Command cond;
   Command thenBody;

   public IFERR(AST def) {
      if(def==null) {
         return;
      }

      AST aCond=def.getFirstChild();
      AST aThen=aCond.getNextSibling();
      cond       =Command.createFromAST(aCond);
      thenBody   =Command.createFromAST(aThen);

   }

   public int getID() {
      return 499;
   }

   public Storable getInstance() {
      return new IFERR(null);
   }

   public void loadState(DataStream ds) {
      cond       =Command.loadFromStorage(ds);
      thenBody   =Command.loadFromStorage(ds);
   }

   public void saveState(DataStream ds) {
      ds.writeInt(cond.getID());
      cond.saveState(ds);
      ds.writeInt(thenBody.getID());
      thenBody.saveState(ds);

   }

   public void exec() throws F8Exception {
      try {
         cond.exec();
      } catch(F8Exception e) {
			thenBody.exec();
      }
		
   }

   public String toString() {
      return "IFERR "+cond.toString()+" THEN "+thenBody.toString()+" END";
   }
}
