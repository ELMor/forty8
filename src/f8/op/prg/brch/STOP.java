package f8.op.prg.brch;

import f8.CL;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class STOP extends NonAlgebraic {
   public STOP() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 47;
   }

   public Storable getInstance() {
      return new STOP();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.pause();
   }

   public String toString() {
      return ("STOP");
   }
}
