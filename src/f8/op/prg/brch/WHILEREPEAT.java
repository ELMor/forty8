package f8.op.prg.brch;

import antlr.collections.AST;
import f8.Command;
import f8.NonAlgebraic;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class WHILEREPEAT extends NonAlgebraic {
   Command whi;
   Command rep;

   public WHILEREPEAT(AST def) {
      if(def==null) {
         return;
      }

      AST wh=def.getFirstChild();
      AST re=wh.getNextSibling();
      whi   =Command.createFromAST(wh);
      rep   =Command.createFromAST(re);
   }

   public int getID() {
      return 10;
   }

   public Storable getInstance() {
      return new WHILEREPEAT(null);
   }

   public void loadState(DataStream ds) {
      whi   =Command.loadFromStorage(ds);
      rep   =Command.loadFromStorage(ds);
   }

   public void saveState(DataStream ds) {
      ds.writeInt(whi.getID());
      whi.saveState(ds);
      ds.writeInt(rep.getID());
      rep.saveState(ds);
   }

   public void exec() throws F8Exception{
      int res=0;

      do {
         whi.exec();
         res=UtilHelper.evalCondition();

         if(res==1) {
            rep.exec();
         } else {
            break;
         }
      } while(true);
   }

   public String toString() {
      return ("WHILE "+whi.toString()+" REPEAT "+rep.toString()+" END");
   }
}
