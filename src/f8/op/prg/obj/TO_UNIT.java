package f8.op.prg.obj;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.Unit;

public final class TO_UNIT extends NonAlgebraic {
   public TO_UNIT() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 508;
   }

   public Storable getInstance() {
      return new TO_UNIT();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("->UNIT");
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkOb#exec()
    */
   public void exec() throws F8Exception {
      CL          cl =StaticCalcGUI.theGUI.getCalcLogica();
      Stackable[] arg=new Stackable[2];
      arg[0]   =new Double(0);
      arg[1]   =new Unit(null);
      String dsp=cl.dispatch(arg);
      if(dsp!=null) {
         cl.push(new Unit(((Double)arg[0]).x, ((Unit)arg[1]).getUnidad()));
      }else{
      	throw new TooFewArgumentsException(this);
      }
   }
}
