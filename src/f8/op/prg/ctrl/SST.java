/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.op.prg.ctrl;

import f8.CL;
import f8.Operation;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.CalcGUI;
import f8.types.utils.Debuggable;

/**
 * @author elinares
 *

 
 */
public class SST extends Operation {

	/* (non-Javadoc)
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CalcGUI cg=StaticCalcGUI.theGUI;
		CL cl=cg.getCalcLogica();
		Debuggable dbg=cl.getProcDebug();
		if(dbg!=null){
			dbg.execDebug(false);
			cg.refresh(false);
			if(dbg.isAnyMore()){
				cg.temporaryLabels(null,dbg.nextCommand());
				cl.setProcDebug(dbg);
			}else{
				cg.temporaryLabels(null,"(returning...)");
			}
			
		}else{
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
