/*
 * Created on 06-oct-2003
 *
 
 
 */
package f8.op.prg.ctrl;

import f8.CL;
import f8.Operation;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;

/**
 * @author elinares
 *

 
 */
public class KILL extends Operation {

	/* (non-Javadoc)
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CL cl=StaticCalcGUI.theGUI.getCalcLogica();
		while(cl.getProcDebug()!=null)
			;
	}

}
