/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.op.prg.ctrl;

import f8.CL;
import f8.Operation;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.CalcGUI;
import f8.types.utils.Debuggable;

/**
 * @author elinares
 *

 
 */
public class DBUG extends Operation {

	/* (non-Javadoc)
	 * @see f8.Operation#exec()
	 */
	public void exec() throws F8Exception {
		CalcGUI cg=StaticCalcGUI.theGUI;
		CL cl=cg.getCalcLogica();
		if(cl.check(1)){
			try{
				if(!cg.getEditField().equals("")){
					cl.enter(cg.getEditField());
				}
				Debuggable dtask=(Debuggable)cl.pop();
				cl.setProcDebug(dtask);
				cg.refresh(true);
				cg.temporaryLabels(null,dtask.nextCommand());
			}catch(Exception e){
				throw new BadArgumentTypeException("Not a program");
			}
		}else{
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
