package f8.op.prg.stk;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class EVAL extends NonAlgebraic {
   public EVAL() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 15;
   }

   public Storable getInstance() {
      return new EVAL();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(1)) {
         Command a=(Command)cl.pop();
         a.exec();
      } else {
			throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("EVAL");
   }
}
