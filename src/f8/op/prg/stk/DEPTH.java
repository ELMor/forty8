package f8.op.prg.stk;

import f8.CL;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class DEPTH extends NonAlgebraic {
   public DEPTH() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 17;
   }

   public Storable getInstance() {
      return new DEPTH();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.push(new Double(cl.size()));
   }

   public String toString() {
      return ("DEPTH");
   }
}
