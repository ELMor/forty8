package f8.op.prg.stk;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.platform.Vector;
import f8.types.Double;

public final class ROLL extends NonAlgebraic {
   public ROLL() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 7;
   }

   public Storable getInstance() {
      return new ROLL();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL  cl =StaticCalcGUI.theGUI.getCalcLogica();
      int rot;
      if(cl.check(1)) {
         Stackable a=cl.pop();
         if(a instanceof Double) {
            rot=(int)((Double)a).x;
            if(cl.check(rot)) {
               Vector gb=UtilFactory.newVector();
               for(int i=0; i<rot; i++) {
                  gb.add(cl.pop());
               }
               for(int i=1; i<rot; i++) {
                  cl.push((Stackable)gb.elementAt(i));
               }
               cl.push((Stackable)gb.elementAt(0));
            } else {
               throw new TooFewArgumentsException(this);
            }
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("ROLL");
   }
}
