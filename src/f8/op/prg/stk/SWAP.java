package f8.op.prg.stk;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class SWAP extends NonAlgebraic {
   public SWAP() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 49;
   }

   public Storable getInstance() {
      return new SWAP();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(2)) {
         Command a=cl.peek(0);
         Command b=cl.peek(1);
         cl.poke(1, a);
         cl.poke(0, b);
      } else {
			throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("SWAP");
   }
}
