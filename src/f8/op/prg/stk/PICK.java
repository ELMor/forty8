package f8.op.prg.stk;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.FunctionDomainException;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class PICK extends NonAlgebraic {
   public PICK() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 16;
   }

   public Storable getInstance() {
      return new PICK();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(1)) {
         Command pick=cl.pop();
         if(pick instanceof Double) {
            int pck=(int)((Double)pick).x;
            if(pck<1){
            	throw new FunctionDomainException(this);
            }
            if(cl.check(pck)) {
               cl.push(cl.peek(pck-1));
            } else {
               throw new TooFewArgumentsException(this);
            }
         } else {
            throw new TooFewArgumentsException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("PICK");
   }
}
