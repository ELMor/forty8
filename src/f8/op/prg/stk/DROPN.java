package f8.op.prg.stk;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class DROPN extends NonAlgebraic {
   public DROPN() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 22;
   }

   public Storable getInstance() {
      return new DROPN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(1)) {
         Command a=cl.pop();
         if(a instanceof Double) {
            int rot=(int)((Double)a).x;
            if(cl.check(rot)) {
               cl.pop(rot);
            } else {
               throw new TooFewArgumentsException(this);
            }
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("DROPN");
   }
}
