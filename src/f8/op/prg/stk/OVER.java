package f8.op.prg.stk;

import f8.CL;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class OVER extends NonAlgebraic {
   public OVER() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 2;
   }

   public Storable getInstance() {
      return new OVER();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(2)) {
         cl.push(cl.peek(1));
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("OVER");
   }
}
