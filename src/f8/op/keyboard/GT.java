package f8.op.keyboard;

import f8.platform.DataStream;
import f8.platform.Storable;

public final class GT extends Comparison {
   public GT() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 84;
   }

   public Storable getInstance() {
      return new GT();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return (">");
   }
   
   
	/* (non-Javadoc)
	 * @see f8.kernel.op.ovl.Comparison#compare(double, double)
	 */
	public boolean compare(double v1, double v2) {
		return v1>v2;
	}

}
