/*
 * Created on 30-ago-2003
 *
 
 
 */
package f8.op.keyboard;

import antlr.CommonAST;
import antlr.Token;
import antlr.collections.AST;
import f8.CL;
import f8.Command;
import f8.Dispatch1;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.platform.MathKernel;
import f8.platform.UtilFactory;
import f8.platform.Vector;
import f8.types.Double;
import f8.types.InfixExp;
import f8.types.Literal;
import f8.types.TempUnit;
import f8.types.Unit;

/**
 * @author elinares
 *
 * Tratamiento de arboles
 */
public final class mkl implements ObjectParserTokenTypes {
   private static double[]   darg=new double[2];
   private static MathKernel mk=UtilFactory.getMath();
   public static final AST   uno=num(1);

   public static AST appendProdChain(AST prod, AST chain) {
      while(chain!=null) {
      }
      return prod;
   }

   /**
    * Comprueba si son numeros.
    * @param n1 Nodo a comprobar
    * @param n2 Nodo a comprobar
    * @return true si n1 y n2 son nodos de tipo Number.
    */
   public static boolean bothNumbers(AST n1, AST n2) {
      if(n1.getType()!=NUMBER) {
         return false;
      }
      if(n2.getType()!=NUMBER) {
         return false;
      }
      darg[0]   =mk.toDouble(n1.getText());
      darg[1]   =mk.toDouble(n2.getText());
      return true;
   }

   /**
    * Crea un nuevo AST sin unir a sus hermanos.
    * @param t El que se quiere clonar
    * @return nuevo t sin hermanos.
    */
   public static AST ccc(AST t) {
      if(t==null) {
         return null;
      }
      AST ret=new CommonAST();
      ret.initialize(t);
      ret.addChild(t.getFirstChild());
      return ret;
   }

   public static AST chainMCD(AST fa1, AST fa2) {
      if((fa1.getType()==MULT)&&(fa2.getType()==MULT)) {
         AST fa11=fa1;
         AST fa21=fa2;
         AST ret =null;
         AST iret=null;
         AST faa1=copyAST(fa11);
         AST faa2=copyAST(fa21);
         AST ch1 =faa1.getFirstChild();
         while(ch1!=null) {
            AST ch2=faa2.getFirstChild();
            while(ch2!=null) {
               if(opEquals(ch1, ch2)) {
                  remove(faa1, ch1);
                  remove(faa2, ch2);
                  if(ret==null) {
                     ret=iret=copyAST(ch1);
                  } else {
                     iret.setNextSibling(copyAST(ch1));
                     iret=iret.getNextSibling();
                  }
               }
               ch2=ch2.getNextSibling();
            }
            ch1=ch1.getNextSibling();
         }
         return ret;
      }
      if((fa1.getType()==MULT)) {
         return chainMCD(fa1, nor(MULT, "*", fa2, num(1)));
      }
      if(fa2.getType()==MULT) {
         return chainMCD(nor(MULT, "*", fa1, num(1)), fa2);
      }
      return chainMCD(nor(MULT, "*", fa1, num(1)), nor(MULT, "*", fa2, num(1)));
   }

   public static AST copyAST(AST t) {
      AST father=new CommonAST();
      AST child;
      father.initialize(t);
      for(child=t.getFirstChild(); child!=null; child=child.getNextSibling()) {
         father.addChild(copyAST(child));
      }
      return father;
   }

   /**
    * Crea un arbol con la expresion derGenerica
    * @param f Nombre de la funcion
    * @param args Lista de argumentos
    * @param var Nombre de la variable sobre la que queremos derivar
    * @return Arbol de derivacion (dF(G(x))/dx -> derF(G(x),dG(x)/dx)
    */
   public static AST derFgen(String f, AST args, String var) throws F8Exception{
      AST nID=new CommonAST(new Token(ID));
      nID.setText(f);
      AST k=args;
      while(k!=null) {
         nID.addChild(ccc(k));
         k=k.getNextSibling();
      }
      k=args;
      while(k!=null) {
         nID.addChild(DER.deriveFunction(k, var));
         k=k.getNextSibling();
      }
      return nID;
   }

   /**
    * Crea una division a1/a2.
    * Si son numeros, devuelve un numero con la division
    * Si el numerador es 0 devuelve 0
    * Si el denominador es 1 devuelve el numerador
    * Se transforma (a11/a12)/a2 => a11/(a2*a12)
    * Se transforma a1/(a21/a22) => (a1*a22)/a21
    * @param a1 Numerador
    * @param a2 Denominador
    * @return Division (con simplificaciones)
    */
   public static AST div(AST up, AST down) {
      if((up==null)&&(down==null)) {
         return num(1); //Fue simplificado
      }
      if(bothNumbers(up, down)) {
         return num(darg[0]/darg[1]); //Esto habr�a que quitarlo?
      }
      if(iComp(up, 0)) {
         return num(0);
      }
      if(iComp(down, 1)) {
         return ccc(up);
      }
      if(opEquals(getBaseOf(up), getBaseOf(down))) {
         AST e1=getExponentOf(up);
         AST e2=getExponentOf(down);
         return ele(getBaseOf(up), res(e1, e2));
      }
      int t1=up.getType();
      int t2=down.getType();
      if((t1==MULT)&&(t2==MULT)) {
         if(removeEqualsFromChainsOfFathers(up, down)){
         	return div(singleMul(up), singleMul(down));
         }
      } else if(t1==MULT) {
         if(removeNodeEquivFromChainOfFather(up, down)) {
            return singleMul(up);
         }
      } else if(t2==MULT) {
         if(removeNodeEquivFromChainOfFather(down, up)) {
            return inv(singleMul(down));
         }
      } else if(t1==DIV) {
         AST nume=up.getFirstChild();
         AST deno=nume.getNextSibling();
         return div(ccc(nume), mul(ccc(deno), ccc(down)));
      } else if(t2==DIV) {
         AST nume=down.getFirstChild();
         AST deno=nume.getNextSibling();
         return div(mul(up, ccc(deno)), ccc(nume));
      }
      return nor(DIV, "/", ccc(up), ccc(down));
   }

   /**
    * Crea un arbol a1^a2
    * Si a1 y a2 son numeros se devuelve un numero
    * Si a1 es 0, se devuelve 0
    * Si a2 es 0, se devuelve 1
    * Si a1 es 1, se devuelve 1
    * Si a2 es 1, se devuelve a1
    * @param a1 base
    * @param a2 exponente
    * @return El arbol de elevacion
    */
   public static AST ele(AST a1, AST a2) {
      if(bothNumbers(a1, a2)) {
         return num(Math.pow(darg[0], darg[1]));
      }
      if(iComp(a1, 0)) {
         return num(0);
      }
      if(iComp(a2, 0)) {
         return num(1);
      }
      if(iComp(a1, 1)) {
         return num(1);
      }
      if(iComp(a2, 1)) {
         return ccc(a1);
      }
      if(isNumber(a2)&&(mk.toDouble(a2.getText())<0)) {
         return div(
                    num(1), ele(a1, num(-mk.toDouble(a2.getText())))
                   );
      }
      return nor(CARET, "^", a1, a2);
   }

   /**
    * Crea un arbol de asignacion
    * @param a1 Izquierdo
    * @param a2 Derecho
    * @return Izquierdo=Derecho
    */
   public static AST equ(AST a1, AST a2) {
      return nor(ASSIGN, "=", a1, a2);
   }

   public static AST expandCaret(AST caret) {
      AST base =caret.getFirstChild();
      AST expon=base.getNextSibling();
      if((expon.getType()==NUMBER)&&(base.getType()!=ID)) {
         MathKernel mk      =UtilFactory.getMath();
         double     exponNum=mk.toDouble(expon.getText());
         if(exponNum>1) {
            return nor(MULT, "*", base, ele(base, num(exponNum-1)));
         }
      }
      return caret;
   }

   public static AST expandMul(AST prod) {
      AST child=prod.getFirstChild();
      AST ret=null;
      while(child!=null) {
         if(child.getType()==PLUS) {
            remove(prod, child);
            prod    =productCardinality(prod);
            child   =copyAST(ccc(child));
            AST sumChild=child.getFirstChild();
            ret=null;
            while(sumChild!=null) {
               AST prdHo=copyAST(prod);
               AST schHo=copyAST(sumChild);
               if(ret==null) {
                  ret=nor(MULT, "*", prdHo, schHo);
               } else {
                  AST retHo=copyAST(ret);
                  ret=nor(PLUS, "+", retHo, nor(MULT, "*", prdHo, schHo));
               }
               sumChild=sumChild.getNextSibling();
            }
            return ret;
         }
         if(child.getType()==MINUS) {
            remove(prod, child);
            AST posi=child.getFirstChild();
            AST nega=posi.getNextSibling();
            return nor(
                       MINUS, "-", nor(MULT, "*", prod, posi),
                       nor(MULT, "*", prod, nega)
                      );
         }
         child=child.getNextSibling();
      }
      return prod;
   }

   private static AST factorizar(AST ch1, AST ch2, AST cel) {
      AST res;
      if(opEquals(cel, ch1)) {
         if(opEquals(cel, ch2)) {
            res=mul(num(2), cel);
         } else {
            AST copyOfCEL=copyAST(cel);
            AST fatherCEL=new CommonAST(new Token(MULT));
            fatherCEL.setFirstChild(copyOfCEL);
            removeEqualsFromChainsOfFathers(ch2, fatherCEL);
            res=mul(sum(num(1), ch2), cel);
         }
      } else {
         if(opEquals(cel, ch2)) {
            AST copyOfCEL=copyAST(cel);
            AST fatherCEL=new CommonAST(new Token(MULT));
            fatherCEL.setFirstChild(copyOfCEL);
            removeEqualsFromChainsOfFathers(ch1, fatherCEL);
            res=mul(sum(num(1), ch1), cel);
         } else {
            AST cel1       =copyAST(cel);
            AST cel2       =copyAST(cel);
            AST fatherOfcel=new CommonAST();
            fatherOfcel.setFirstChild(cel1);
            removeEqualsFromChainsOfFathers(ch1, fatherOfcel);
            fatherOfcel.setFirstChild(cel2);
            removeEqualsFromChainsOfFathers(ch2, fatherOfcel);
            res=mul(sum(ch1, ch2), cel);
         }
      }
      return res;
   }

   /**
    * Crea un arbol de llamada a funcion
    * @param f Nombre de la funcion
    * @param a1 Argumento
    * @return Devuelve el arbol que representa f(a1)
    */
   public static AST fca(String f, AST a1) {
      AST nID=new CommonAST(new Token(ID));
      nID.setText(f);
      CL      cl=StaticCalcGUI.theGUI.getCalcLogica();
      Command k=cl.lookup(f);
      if((k!=null)&&k instanceof Dispatch1) {
         Dispatch1 fk   =(Dispatch1)k;
         String   undoF=fk.undo();
         if(a1!=null && undoF!=null && undoF.equals(a1.getText())) {
            return a1.getFirstChild();
         }
      }
      if(a1!=null) {
         nID.addChild(ccc(a1));
      }
      return nID;
   }

   /**
    * Crea un arbol de llamada a funcion
    * @param f Nombre de la funcion
    * @param a1 Primer argumento
    * @param a2 Segundo argumento
    * @return Devuelve el arbol f(a1,a2)
    */
   public static AST fca2(String f, AST a1, AST a2) {
      AST nID=new CommonAST(new Token(ID));
      nID.setText(f);
      nID.addChild(ccc(a1));
      nID.addChild(ccc(a2));
      return nID;
   }

   /**
    * Crea un arbol de llamada a funcion
    * @param f Nombre de la funcion
    * @param args Lista de argumentos
    * @return Devuelve el arbol f(args,arg.getNextSibling(),...)
    */
   public static AST fun(String f, AST headOfChain) {
      return opSiblings(ID, f, headOfChain);
   }

   public static AST getBaseOf(AST op) {
      switch(op.getType()) {
         case CARET:
            return op.getFirstChild();
      }
      return op;
   }

   public static AST getExponentOf(AST op) {
      switch(op.getType()) {
         case CARET:
            return op.getFirstChild().getNextSibling();
      }
      return num(1);
   }

   /**
    * Devuelve true si ambos nodos son numeros iguales
    * @param n1 Primer nodo
    * @param n2 Segundo nodo
    * @return true si ambos son numeros e  iguales
    */
   public static boolean iComp(AST n1, AST n2) {
      if((n1.getType()!=NUMBER)||(n2.getType()!=NUMBER)) {
         return false;
      }
      return mk.toDouble(n1.getText())==mk.toDouble(n2.getText());
   }

   /**
    * Comprueba si d y n son iguales
    * @param n Nodo
    * @param d double
    * @return true si el numero que representa el nod es igual a d
    */
   public static boolean iComp(AST n, double d) {
      if(n.getType()!=NUMBER) {
         return false;
      }
      double n2=UtilFactory.getMath().toDouble(n.getText());
      return n2==d;
   }

   public static AST insertBefore(AST father, AST child, AST newchild) {
      AST k =father.getFirstChild();
      AST lk;
      if(k==child) {
         father.setFirstChild(newchild);
         newchild.setNextSibling(k);
         return father;
      }
      lk=child;
      for(k=k.getNextSibling(); k!=null; k=k.getNextSibling()) {
         if(k==child) {
            lk.setNextSibling(newchild);
            newchild.setNextSibling(k);
            return father;
         }
         lk=k;
      }
      return father;
   }

   /**
    * Devuelve el inverso de la expresion
    * Si a1 es una division (num/den), se develve (den/num)
    * Si a1 es INV(b) se devuelve b
    * @param a1 Expresion a invertir
    * @return INV(a1)
    */
   public static AST inv(AST a1) {
      if(a1.getType()==DIV) {
         AST nume=a1.getFirstChild();
         AST deno=nume.getNextSibling();
         return div(deno, nume);
      } else if(a1.getType()==INV) {
         return a1.getFirstChild();
      } else {
         AST ret=new CommonAST(new Token(INV));
         ret.addChild(ccc(a1));
         return ret;
      }
   }

   /**
    * Comprueba si es un numero.
    * @param n1 Nodo a comprobar
    * @return true si n1 es un nodo de tipo Number.
    */
   public static boolean isNumber(AST n1) {
      if(n1.getType()!=NUMBER) {
         return false;
      }
      darg[0]=mk.toDouble(n1.getText());
      return true;
   }

   public static AST joinProducts(AST mp) {
      if(mp.getType()!=MULT) {
         return mp; //No hacemos nada
      }
      AST child=mp.getFirstChild();
      while(child!=null) {
         if(child.getType()==MULT) {
            remove(mp, child);
            child=joinProducts(child);
            mp.addChild(child.getFirstChild());
         }
         child=child.getNextSibling();
      }
      return mp;
   }

   public static AST joinSumas(AST ms) {
      if(ms.getType()!=PLUS) {
         return ms; //No hacemos nada
      }
      AST child=ms.getFirstChild();
      while(child!=null) {
         if(child.getType()==PLUS) {
            remove(ms, child);
            child=joinSumas(child);
            ms.addChild(child.getFirstChild());
         }
         child=child.getNextSibling();
      }
      return ms;
   }

   /**
    * Devuelve el producto de los nodos
    * Si a1 o a2 == 0, devuelve 0
    * Si ambos son numeros, se devuelve un numero
    * Si a1==1, se devuelve a2
    * Si a2==1, se devuelve a1
    * Si a1==INV(a2), se devuelve 1
    * Si a1 o a2 es un producto, se a�ade a2 o a1 al mismo
    * Si a1=(numerador/den) y a2=den, se devuelve numerador
    * @param a1 Primer nodo
    * @param a2 Segundo nodo
    * @return nodo que representa el producto de a1 y a2
    */
   public static AST mul(AST a1, AST a2) {
      return singleMul(joinProducts(nor(MULT, "*", a1, a2)));
   }

   /**
    * Se devuelve un cambio de signo
    * Si a1==NEG(b), se devuelve b.
    * @param a1 Nodo que se quiere negativizar
    * @return -a1
    */
   public static AST neg(AST a1) {
      if(a1.getType()==NEG) {
         return ccc(a1.getFirstChild());
      }
      AST ret=new CommonAST(new Token(NEG));
      ret.addChild(ccc(a1));
      return ret;
   }

   public static AST nodeEquivInChain(AST chain, AST node) {
      for(; chain!=null; chain=chain.getNextSibling()) {
         if(opEquals(node, chain)) {
            return chain;
         }
      }
      return null;
   }

   /**
    * DEvuelve un nodo operador infijo (+,-,*,/,^)
    * @param tipo (ver ObjectParserTokenTypes)
    * @param f Nombre del operador
    * @param a1 Izquierdo
    * @param a2 Derecho
    * @return Nodo que representa la operacion
    */
   public static AST nor(int tipo, String f, AST a1, AST a2) {
      AST exp=new CommonAST(new Token(tipo));
      exp.setText(f);
      exp.addChild(ccc(a1));
      exp.addChild(ccc(a2));
      return exp;
   }

   /**
    * Crea un nodo de tipo numero
    * @param number Valor
    * @return Nodo de tipo numero que representa Valor.
    */
   public static AST num(double number) {
      AST exp=new CommonAST(new Token(NUMBER));
      exp.setText(""+number);
      return exp;
   }

   public static boolean opEquals(AST n1, AST n2) {
      int t1=n1.getType();

      //Si no son del mismo tipo, no son iguales
      if(t1!=n2.getType()) {
         return false;
      }
      AST ch11=n1.getFirstChild();
      AST ch12=null;
      AST ch21=n2.getFirstChild();
      AST ch22=null;

      if(ch11!=null) {
         ch12=ch11.getNextSibling();
      }
      if(ch21!=null) {
         ch22=ch21.getNextSibling();
      }

      if(ch11==null) {
         if(t1==NUMBER) {
            MathKernel mk=UtilFactory.getMath();
            return mk.toDouble(n1.getText())==mk.toDouble(n2.getText());
         }
         return n1.getText().equals(n2.getText());
      }

      //Obviamente
      if(ch11==ch21) {
         return true;
      }
      if(((ch12==null)&&(ch22!=null))||((ch11!=null)&&(ch21==null))) {
         return false;
      }
      switch(t1) {
         case NEG:
         case INV:
            return opEquals(ch11, ch21);
         case MULT:
         case PLUS:
            return unorderedChainsEquals(n1, n2);
         case ID:
            if(!n1.getText().equals(n2.getText())) {
               return false;
            }
            return orderedChainsEquals(n1, n2);
      }
      return opEquals(ch11, ch21)&&opEquals(ch12, ch22);
   }

   public static AST opSiblings(int tipo, String text, AST t) {
      if(t==null) {
         return null;
      }
      AST ret=new CommonAST(new Token(tipo));
      ret.setText(text);
      while(t!=null) {
         ret.addChild(t);
         t=t.getNextSibling();
      }
      return ret;
   }

   public static boolean orderedChainsEquals(AST f1, AST f2) {
      int sz1=f1.getNumberOfChildren();
      if(sz1!=f2.getNumberOfChildren()) {
         return false;
      }
      AST p1=f1.getFirstChild();
      AST p2=f2.getFirstChild();
      while(p1!=null) {
         if(!opEquals(p1, p2)) {
            return false;
         }
         p1   =p1.getNextSibling();
         p2   =p2.getNextSibling();
      }
      return true;
   }

   private static AST productCardinality(AST prod) {
      int nc=prod.getNumberOfChildren();
      if(nc==0) {
         return num(1);
      }
      if(nc==1) {
         return prod.getFirstChild();
      }
      return prod;
   }

   public static AST remove(AST father, AST child) {
      AST chainPointer=father.getFirstChild();
      if(chainPointer==null) {
         return father;
      }
      AST lastPointer;
      if(chainPointer==child) {
         father.setFirstChild(chainPointer.getNextSibling());
         return father;
      }
      lastPointer=chainPointer;
      for(
          chainPointer=chainPointer.getNextSibling(); chainPointer!=null;
              chainPointer=chainPointer.getNextSibling()
         ) {
         if(chainPointer==child) {
            lastPointer.setNextSibling(chainPointer.getNextSibling());
            return father;
         }
         lastPointer=chainPointer;
      }
      return father;
   }

   public static boolean removeEqualsFromChainsOfFathers(AST fa1, AST fa2) {
      AST ch1=fa1.getFirstChild();
      AST ch2=fa2.getFirstChild();
      AST p1 =ch1;
      AST p2 =ch2;
      boolean changed=false;
      while(p2!=null) {
         p1=caretNodeEquivInChain(ch1, p2);
         if(p1!=null) {
            AST exp1=getExponentOf(p1);
            AST exp2=getExponentOf(p2);
            subst(fa1, p1, mkl.ele(getBaseOf(p1), mkl.res(exp1, exp2)));
            remove(fa2, p2);
            changed=true;
         }
         if(p2!=null) {
            p2=p2.getNextSibling();
         }
      }
      return changed;
   }

   public static AST caretNodeEquivInChain(AST chain, AST node) {
      AST nodeBase=getBaseOf(node);
      while(chain!=null) {
         AST nodeChainBase=getBaseOf(chain);
         if(opEquals(nodeBase, nodeChainBase)) {
            return chain;
         }
         chain=chain.getNextSibling();
      }
      return null;
   }

   public static boolean removeNodeEquivFromChainOfFather(AST fa1, AST node) {
      AST p1=caretNodeEquivInChain(fa1.getFirstChild(), node);
      if(p1!=null) {
         AST exp1=getExponentOf(p1);
         AST exp2=getExponentOf(node);
         subst(fa1, p1, mkl.ele(getBaseOf(p1), mkl.res(exp1, exp2)));
         return true;
      }
      return false;
   }

   public static AST res(AST posi, AST nega) {
      if((posi==null)&&(nega==null)) {
         return num(0);
      }
      if(bothNumbers(posi, nega)) {
         return num(darg[0]-darg[1]);
      }
      if(iComp(posi, 0)) {
         return neg(ccc(nega));
      }
      if(iComp(nega, 0)) {
         return ccc(posi);
      }
      if(opEquals(posi, nega)) {
         return num(0);
      }
      if(opEquals(posi, neg(nega))) {
         return mul(num(2), posi);
      }
      int t1=posi.getType();
      int t2=nega.getType();
      if((t1==PLUS)&&(t2==PLUS)) {
         removeEqualsFromChainsOfFathers(posi, nega);
         return nor(MINUS, "-", singleSum(posi), singleSum(nega));
      } else if(t1==PLUS) {
         if(removeNodeEquivFromChainOfFather(posi, nega)) {
            return singleSum(posi);
         }
      } else if(t2==PLUS) {
         if(removeNodeEquivFromChainOfFather(nega, posi)) {
            return neg(singleSum(nega));
         }
      } else if(t1==MINUS) {
         AST posiposi=posi.getFirstChild();
         AST posinega=posiposi.getNextSibling();
         return res(ccc(posiposi), sum(ccc(posinega), ccc(nega)));
      } else if(t2==MINUS) {
         AST negaposi=nega.getFirstChild();
         AST neganega=negaposi.getNextSibling();
         return res(sum(posi, ccc(neganega)), ccc(negaposi));
      }
      return nor(MINUS, "-", ccc(posi), ccc(nega));
   }

   public static AST singleMul(AST prod) {
      AST ch1;

      if(prod.getNumberOfChildren()==0) {
         return num(1);
      }

      //Primero subir a este nivel los productos que haya dentro
      //y aupar las divisiones
      ch1=prod.getFirstChild();
      while(ch1!=null) {
         if(iComp(ch1, 0)) {
            return num(0);
         }
         if(iComp(ch1, 1)) {
            remove(prod, ch1);
            ch1=ch1.getNextSibling();
            continue;
         }
         if(ch1.getType()==MULT) {
            remove(prod, ch1);
            return mul(prod, ccc(ch1));
         }
         if(ch1.getType()==DIV) {
            AST nume=ch1.getFirstChild();
            AST deno=nume.getNextSibling();
            remove(prod, ch1);
            return div(mul(prod, ccc(nume)), ccc(deno));
         }
         if(ch1.getType()==INV) {
            AST deno=ch1.getFirstChild();
            removeNodeEquivFromChainOfFather(prod, deno);
         }
         ch1=ch1.getNextSibling();
      }

      //No hay productos dentro de este producto, ni divisiones
      //Comprobamos las igualdades entre operandos
      ch1=prod.getFirstChild();
      while(ch1!=null) {
         //Ahora se solucionan los numeros literales 
         AST ch2=ch1.getNextSibling();
         for(; ch2!=null; ch2=ch2.getNextSibling()) {
            if(bothNumbers(ch1, ch2)) {
               subst(prod, ch1, num(darg[0]*darg[1]));
               remove(prod, ch2);
               continue;
            }

            //Dos iguales
            AST base=getBaseOf(ch1);
            if(opEquals(base, getBaseOf(ch2))) {
               AST e1    =getExponentOf(ch1);
               AST e2    =getExponentOf(ch2);
               AST newch1=ele(base, sum(e1, e2));
               subst(prod, ch1, newch1);
               remove(prod, ch2);
               ch1=newch1;
               continue;
            }
         }
         ch1=ch1.getNextSibling();
      }

      return productCardinality(prod);
   }

   public static AST singleSum(AST suma) {
      AST ch1;

      if(suma.getNumberOfChildren()==0) {
         return num(0);
      }

      //Primero subir a este nivel los productos que haya dentro
      //y aupar las divisiones
      ch1=suma.getFirstChild();
      while(ch1!=null) {
         if(iComp(ch1, 0)) {
            remove(suma, ch1);
            ch1=ch1.getNextSibling();
            continue;
         }
         if(ch1.getType()==PLUS) {
            remove(suma, ch1);
            return sum(suma, ccc(ch1));
         }
         if(ch1.getType()==MINUS) {
            AST posi=ch1.getFirstChild();
            AST nega=posi.getNextSibling();
            remove(suma, ch1);
            return res(sum(suma, ccc(posi)), ccc(nega));
         }
         if(ch1.getType()==NEG) {
            AST nega=ch1.getFirstChild();
            removeNodeEquivFromChainOfFather(suma, nega);
         }
         ch1=ch1.getNextSibling();
      }

      //No hay productos dentro de este producto, ni divisiones
      //Comprobamos las igualdades entre operandos
      ch1=suma.getFirstChild();
      while(ch1!=null) {
         //Ahora se solucionan los numeros literales 
         AST ch2=ch1.getNextSibling();
         for(; ch2!=null; ch2=ch2.getNextSibling()) {
            if(bothNumbers(ch1, ch2)) {
               subst(suma, ch1, num(darg[0]+darg[1]));
               remove(suma, ch2);
               continue;
            }

            //Suma de productos del mismo coeficiente
            AST cel=chainMCD(ch1, ch2);
            if((cel!=null)&&(cel.getType()!=NUMBER)) {
               AST res=factorizar(ch1, ch2, cel);
               subst(suma, ch1, res);
               remove(suma, ch2);
               ch1=res;
               continue;
            }
         }
         ch1=ch1.getNextSibling();
      }
      return sumaCardinality(suma);
   }

   /**
    * Substitucion de un nodo por otro
    * @param father El padre
    * @param child El hijo a sustituir
    * @param newchild El nuevo hijo. Se coloca en el mismo lugar que el anterior.
    * @return El nuevo arbol con el hijo sustituido (si se encontr�)
    */
   public static AST subst(AST father, AST child, AST newchild) {
      AST chainPointer=father.getFirstChild();
      AST lastPointer;
      if(chainPointer==child) {
         father.setFirstChild(newchild);
         newchild.setNextSibling(chainPointer.getNextSibling());
         return father;
      }
      lastPointer=chainPointer;
      for(
          chainPointer=chainPointer.getNextSibling(); chainPointer!=null;
              chainPointer=chainPointer.getNextSibling()
         ) {
         if(chainPointer==child) {
            lastPointer.setNextSibling(newchild);
            newchild.setNextSibling(chainPointer.getNextSibling());
            return father;
         }
         lastPointer=chainPointer;
      }
      return father;
   }

   public static AST sum(AST a1, AST a2) {
      return singleSum(joinSumas(nor(PLUS, "+", a1, a2)));
   }

   private static AST sumaCardinality(AST suma) {
      int nc=suma.getNumberOfChildren();
      if(nc==1) {
         return suma.getFirstChild();
      }
      if(nc==0) {
         return num(0);
      }
      return suma;
   }

   public static boolean unorderedChainsEquals(AST father1, AST father2) {
      int sz1=father1.getNumberOfChildren();
      if(sz1!=father2.getNumberOfChildren()) {
         return false;
      }
      Vector nodeList1=UtilFactory.newVector();
      AST    aux=father1.getFirstChild();
      while(aux!=null) {
         nodeList1.add(aux);
         aux=aux.getNextSibling();
      }
      AST headOfChain2=father2.getFirstChild();
      AST found;
      do {
         found=nodeEquivInChain(headOfChain2, (AST)nodeList1.elementAt(0));
         nodeList1.removeElementAt(0);
      } while((nodeList1.size()>0)&&(found!=null));

      return found!=null;
   }

   public static Vector incogOf(CL cl, AST exp) {
      Vector ret=UtilFactory.newVector();
      recSearch(ret, cl, exp);
      return ret;
   }

   public static void recSearch(Vector v, CL cl, AST e) {
      AST child;
      switch(e.getType()) {
         default:
            child=e.getFirstChild();
            while(child!=null) {
               recSearch(v, cl, child);
               child=child.getNextSibling();
            }
            break;
         case NUMBER:
         case UNIT:
            break;
         case ID:
            if(e.getNumberOfChildren()==0) {
               Command s=cl.lookup(e.getText());
               if(s==null) {
                  s=new Literal(e.getText());
               }
               if(s instanceof InfixExp) {
                  recSearch(v, cl, ((InfixExp)s).getAST());
               } else if(s instanceof Literal) {
                  if(!((Literal)s).nombre.equals(e.getText())) {
                     recSearch(v, cl, ((Literal)s).getAST());
                  } else {
                     asSetAdd(v, e.getText());
                  }
               } else if(s instanceof Double) {
                  asSetAdd(v, e.getText());
               } else if(s instanceof Unit){
						asSetAdd(v, e.getText());
               } else if(s instanceof TempUnit){
						asSetAdd(v, e.getText());
               }
            } else {
               child=e.getFirstChild();
               while(child!=null) {
                  recSearch(v, cl, child);
                  child=child.getNextSibling();
               }
            }
      }
   }

   private static void asSetAdd(Vector v, Object o) {
      if(v.find(o)<0) {
         v.add(o);
      }
   }
}
