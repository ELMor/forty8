package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.InfixExp;
import f8.types.Unit;

public final class CARET extends Dispatch2 implements Derivable {
   public CARET() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST exp, String var) throws F8Exception {
      AST a1=exp;
      AST a2=a1.getNextSibling();
      return mkl.sum(
                     mkl.mul(
                             mkl.mul(DER.deriveFunction(a1, var), a2),
                             mkl.ele(a1, mkl.res(a2, mkl.num(1)))
                            ),
                     mkl.mul(
                             mkl.mul(
                                     mkl.fca("LN", a1),
                                     DER.deriveFunction(a2, var)
                                    ), mkl.ele(a1, a2)
                            )
                    );
   }

   public int getID() {
      return 56;
   }

   public Storable getInstance() {
      return new CARET();
   }

   public int getTipo() {
      return ObjectParserTokenTypes.CARET;
   }

   public void loadState(DataStream ds) {
   }

   // x^y
   public Stackable prfDoubleDouble(Double x, Double y) {
      if(x.x>0.0) {
      	return new Double(Math.pow(x.x,y.x));
      } else if(x.x<0.0) {
         int    n  =(int)Math.floor(y.x);
         double ell=Math.log(-x.x);

         if((n&1)==0) {
            return new Double(Math.exp(y.x*ell));
         } else {
            return new Double(-Math.exp(y.x*ell));
         }
      } else {
         return new Double(0.0);
      }
   }
	/* (non-Javadoc)
	 * @see f8.kernel.FunArgArg#prfInfixExpDouble(f8.kernel.types.InfixExp, f8.kernel.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b) {
		return new InfixExp(mkl.ele(a.getAST(), mkl.num(b.x)));
	}

	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b){
		return new InfixExp(mkl.ele(a.getAST(), b.getAST()));
	}


   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn2#f(f8.kernel.types.Unit, double)
    */
   public Stackable prfUnitDouble(Unit un, Double vexp) {
      return (new Unit(
                       Math.pow(un.getValor(), vexp.x),
                       mkl.ele(un.getUnidad(), mkl.num(vexp.x))
                      ));
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("^");
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex, f8.kernel.types.Complex)
	 */
	public Stackable prfComplexComplex(Complex a, Complex b)
		throws F8Exception {
		return a.power(b);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfComplexDouble(f8.kernel.types.Complex, f8.kernel.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return a.power(b.x);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfDoubleComplex(f8.kernel.types.Double, f8.kernel.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return new Complex(a.x,0).power(b);
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
		throws F8Exception {
			return new InfixExp(mkl.mul(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b)
		throws F8Exception {
			return new InfixExp(mkl.mul(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
		throws F8Exception {
			return new InfixExp(mkl.mul(a.getAST(),b.getAST()));
	}

}
