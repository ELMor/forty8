package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;

public final class LN extends Dispatch1 implements Derivable  {
   public LN() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   public int getID() {
      return 102;
   }

   public Storable getInstance() {
      return new LN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable prfDouble(Double x) throws F8Exception {
   	if(x.x<0)
   		return new Complex(x.x,0).log();
      return new Double(Math.log(x.x));
   }

   public String toString() {
      return ("LN");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST arg, String var) throws F8Exception {
      return mkl.div(DER.deriveFunction(arg, var), arg);
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "EXP";
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.log();
	}

}
