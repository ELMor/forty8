package f8.op.keyboard;

import antlr.collections.AST;
import f8.Stackable;
import f8.Trigonometric;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;

public final class SIN extends Trigonometric {
   public SIN() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   public int getID() {
      return 58;
   }

   public Storable getInstance() {
      return new SIN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable prfDouble(Double x) {
      return new Double(Math.sin(Settings2RAD(x.x)));
   }

   public String toString() {
      return ("SIN");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST)
    */
   public AST deriveWithArgs(AST args, String var) throws F8Exception {
      return mkl.mul(mkl.fca("COS", args), DER.deriveFunction(args, var));
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "ASIN";
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.sin();
	}

}
