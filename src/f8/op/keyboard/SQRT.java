package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.Unit;

public final class SQRT extends Dispatch1 implements Derivable  {
   public SQRT() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 59;
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   public Storable getInstance() {
      return new SQRT();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable prfDouble(Double x) {
   	if(x.x<0){
   		return new Complex(0,Math.sqrt(-x.x));
   	}else{
			return new Double(Math.sqrt(x.x));
   	}
   }

   public String toString() {
      return ("\u0083");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST arg, String var) throws F8Exception {
      return mkl.div(
                     DER.deriveFunction(arg, var),
                     mkl.mul(mkl.num(2), mkl.fca("\u0083", arg))
                    );
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#f(f8.kernel.types.Unit)
    */
   public Unit perform(Unit a) {
      return new Unit(
                      Math.sqrt(a.getValor()),
                      mkl.ele(a.getUnidad(), mkl.num(0.5))
                     );
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "SQ";
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.sqrt();
	}

}
