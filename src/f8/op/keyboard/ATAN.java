package f8.op.keyboard;

import antlr.collections.AST;
import f8.Stackable;
import f8.Trigonometric;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.Unit;

public final class ATAN extends Trigonometric {
   public ATAN() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 52;
   }

   /* (non-Javadoc)
    * @see f8.kernel.Command#isAlgebraic()
    */
   public boolean isAlgebraic() {
      return true;
   }

   public Storable getInstance() {
      return new ATAN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable prfDouble(Double x) {
      if((x.x<-1)||(x.x>1)) {
         return new Complex(x.x, 0).atan();
      }
      return new Double(RAD2Settings(Math.atan(x.x)));
   }

   public String toString() {
      return ("ATAN");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST args, String var) throws F8Exception {
      return mkl.mul(
                     DER.deriveFunction(args, var),
                     mkl.div(
                             mkl.num(1),
                             mkl.sum(mkl.num(1), mkl.ele(args, mkl.num(2)))
                            )
                    );
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#f(f8.kernel.types.Unit)
    */
   public Unit perform(Unit a) {
   	// TODO arcotangente con unidades
      return null;
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "TAN";
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.atan();
	}

}
