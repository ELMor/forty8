/*
 * Created on 17-ago-2003
 *
 
 
 */
package f8.op.keyboard;

import antlr.CommonAST;
import antlr.collections.AST;
import f8.CL;
import f8.Command;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.op.prg.stk.EVAL;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.InfixExp;
import f8.types.Literal;

/**
 * @author elinares
 *

 
 */
public final class Sigma extends Command {
	AST full;

   public Sigma(AST tree) {
   	full=tree;
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   public void exec() throws F8Exception{
   	CL cl=StaticCalcGUI.theGUI.getCalcLogica();
		if(cl.check(4)){
			Stackable var=cl.peek(3);
			Stackable from=cl.peek(2);
			Stackable to=cl.peek(1);
			Stackable exp=cl.peek(0);
			String varName=null;
			if((var instanceof Literal || (varName=InfixExp.isLiteral(var))!=null)  &&
			   (from instanceof Double || from instanceof InfixExp) &&
			   (to instanceof Double || to instanceof InfixExp)     &&
			   (exp instanceof Double || exp instanceof InfixExp)){
				cl.pop(4);
				
				cl.push(from);
				new EVAL().exec();
				Stackable nfrom=cl.pop();
				
				cl.push(to);
				new EVAL().exec();
				Stackable nto=cl.pop();
				
				if(nfrom instanceof Double && nto instanceof Double){
					String prg=" 0 ";
					prg+=nfrom.toString()+" ";
					prg+=nto.toString()+" ";
					prg+="FOR "+varName+" "+exp.toString()+" EVAL + NEXT ";
					cl.enter(prg);
				}else{
					AST sigma=new CommonAST();
					sigma.setText("\u0085");
					sigma.setType(SIGMA);
					sigma.addChild(var.getAST());
					sigma.addChild(from.getAST());
					sigma.addChild(to.getAST());
					sigma.addChild(exp.getAST());
					cl.push(new InfixExp(sigma));
				}
			}else{
				throw new BadArgumentTypeException(this);
			}
		}else{
			throw new TooFewArgumentsException(this);
		}
   }

   /* (non-Javadoc)
    * @see f8.pfr.io.Storable#getID()
    */
   public int getID() {
      return 1117;
   }

   /* (non-Javadoc)
    * @see f8.pfr.io.Storable#getInstance()
    */
   public Storable getInstance() {
      return new Sigma(null);
   }

   /* (non-Javadoc)
    * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
    */
   public void saveState(DataStream ds) {
		Command.saveState(ds,full);
   }

   /* (non-Javadoc)
    * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
    */
   public void loadState(DataStream ds) {
   	AST[] nodes=new AST[1];
		Command.loadState(ds,nodes); 
		full=nodes[0];
   }

   public String toString() {
      return "\u0085";
   }
   
   public static String forma(AST sigma){
   	AST child=sigma.getFirstChild();
		String sigId=InfixExp.forma(child);
		child=child.getNextSibling();
		String sigDo=InfixExp.forma(child);
		child=child.getNextSibling();
		String sigUp=InfixExp.forma(child);
		child=child.getNextSibling();
		String sigFu=InfixExp.forma(child); 
		return "\u0085("+sigId+"="+sigDo+","+sigUp+","+sigFu+")";

   }
	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
