package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class ALOG extends Dispatch1 implements Derivable  {
   public ALOG() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 511;
   }

   public Storable getInstance() {
      return new ALOG();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable prfDouble(Double x) {
		return new Double(Math.pow(10, x.x));
   }

   public String toString() {
      return ("ALOG");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST arg, String var) throws F8Exception{
      return mkl.div(
                     mkl.mul(
                             DER.deriveFunction(arg, var),
                             mkl.num(1/Math.log(Math.E))
                            ), arg
                    );
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "LOG";
   }
   
   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   
}
