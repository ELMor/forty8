package f8.op.keyboard;

import antlr.collections.AST;
import f8.Dispatch1;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;

public final class EXP extends Dispatch1 implements Derivable  {
   public EXP() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
	* @see f8.kernel.Command#isAlgebraic()
	*/
   public boolean isAlgebraic() {
	   return true;
   }
   public int getID() {
      return 74;
   }

   public Storable getInstance() {
      return new EXP();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable prfDouble(Double x) throws F8Exception {
      return new Double(Math.exp(x.x));
   }

   public String toString() {
      return ("EXP");
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST, java.lang.String)
    */
   public AST deriveWithArgs(AST arg, String var) throws F8Exception {
      return mkl.mul(DER.deriveFunction(arg, var), mkl.fca("EXP", arg));
   }

   /* (non-Javadoc)
    * @see f8.kernel.op.num.Fcn#undo()
    */
   public String undo() {
      return "LN";
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return new Complex(Math.E,0).power(x);
	}

}
