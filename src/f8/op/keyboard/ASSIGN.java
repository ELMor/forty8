package f8.op.keyboard;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Complex;
import f8.types.Double;
import f8.types.InfixExp;
import f8.types.Unit;

public final class ASSIGN extends Dispatch2 {
   public ASSIGN() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 13;
   }

   public Storable getInstance() {
      return new ASSIGN();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("=");
   }
   
   private Stackable ret(Stackable a, Stackable b){
		return new InfixExp(mkl.equ(a.getAST(), b.getAST()));
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfComplexInfixExp(f8.kernel.types.Complex, f8.kernel.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
		throws F8Exception {
		return ret(a,b);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfDoubleInfixExp(f8.kernel.types.Double, f8.kernel.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b)
		throws F8Exception {
			return ret(a,b);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfInfixExpComplex(f8.kernel.types.InfixExp, f8.kernel.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
		throws F8Exception {
			return ret(a,b);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfInfixExpDouble(f8.kernel.types.InfixExp, f8.kernel.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b)
		throws F8Exception {
			return ret(a,b);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Dispatch2#prfInfixExpInfixExp(f8.kernel.types.InfixExp, f8.kernel.types.InfixExp)
	 */
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
		throws F8Exception {
			return ret(a,b);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfInfixExpUnit(f8.types.InfixExp, f8.types.Unit)
	 */
	public Stackable prfInfixExpUnit(InfixExp a, Unit b) throws F8Exception {
		return new InfixExp(mkl.equ(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfUnitInfixExp(f8.types.Unit, f8.types.InfixExp)
	 */
	public Stackable prfUnitInfixExp(Unit a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.equ(a.getAST(),b.getAST()));
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfUnitUnit(f8.types.Unit, f8.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		return new InfixExp(mkl.equ(a.getAST(),b.getAST()));
	}

}
