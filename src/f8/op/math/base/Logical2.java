/*
 * Created on 01-oct-2003
 *
 
 
 */
package f8.op.math.base;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.types.Double;

/**
 * @author elinares
 *

 
 */
public abstract class Logical2 extends Dispatch2 {
	public abstract double logic(double l, double r);
	
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		return  new Double(logic(a.x,b.x));
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfixOperator()
	 */
	public boolean isInfixOperator() {
		return true;
	}

	

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
