package f8.op.math.vectr;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.F8Vector;

public final class FLECHAV2 extends Dispatch2 {
   public FLECHAV2() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3041;
   }

   public Storable getInstance() {
      return new FLECHAV2();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("\u008DV2");
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		double[] d=new double[2];
		d[0]=a.x;
		d[1]=b.x;
		return new F8Vector(d);
	}

}
