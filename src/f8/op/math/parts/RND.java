package f8.op.math.parts;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class RND extends Dispatch2 {
   public RND() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3015;
   }

   public String toString() {
	  return ("RND");
   }

   public Storable getInstance() {
      return new RND();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		double mant=Math.round(a.x*Math.pow(10,(int)b.x+1));
		return new Double(mant/Math.pow(10,(int)b.x+1));		
	}

	/* (non-Javadoc)
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
