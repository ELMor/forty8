package f8.op.math.prob;

import f8.Dispatch2;
import f8.Stackable;
import f8.kernel.rtExceptions.BadArgumentValueException;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;

public final class PERM extends Dispatch2 {
   public PERM() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 3020;
   }

   public String toString() {
	  return ("PERM");
   }

   public Storable getInstance() {
      return new PERM();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		int n=(int)a.x,r=(int)b.x;
		if(n<r)
			throw new BadArgumentValueException(this);
		double ret=1;
		for(int i=n;i>1;i--)
			ret*=i;
		return new Double(ret/(n-r));
	}

}
