package f8.op.units;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.Unit;

public final class UVAL extends NonAlgebraic {
   public UVAL() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 2484;
   }

   public Storable getInstance() {
      return new UVAL();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("UVAL");
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkOb#exec()
    */
   public void exec() throws TooFewArgumentsException{
      CL          cl =StaticCalcGUI.theGUI.getCalcLogica();
      Stackable[] arg=new Stackable[1];
      arg[0]=new Unit(null);
      String dsp=cl.dispatch(arg);
      if(dsp!=null) {
         Unit ar=(Unit)arg[0];
         cl.push(new Double(ar.getValor()));
      }else{
      	throw new TooFewArgumentsException(this);
      }
   }
}
