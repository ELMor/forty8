package f8.op.units;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.TempUnit;
import f8.types.Unit;

public final class CONVERT extends NonAlgebraic {
   public CONVERT() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 104;
   }

   public Storable getInstance() {
      return new CONVERT();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public String toString() {
      return ("CONVERT");
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkOb#exec()
    */
   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(2)) {
         Stackable a=cl.peek(1);
         Stackable b=cl.peek(0);
         if(
            (a instanceof Unit||a instanceof TempUnit)
                &&(b instanceof Unit||b instanceof TempUnit)
           ) {
            Unit from=(Unit)a;
            Unit to=(Unit)b;
            Unit converted=from.convert(to);
            cl.pop(2);
            cl.push(converted);
         } else {
			throw new BadArgumentTypeException(this);
         }
      } else {
		throw new TooFewArgumentsException(this);
      }
   }
}
