package f8.modes;


import f8.CL;
import f8.kernel.rtExceptions.F8Exception;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class FIX extends CheckGroup {
   public FIX() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 75;
   }

   public Storable getInstance() {
      return new FIX();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() throws F8Exception{
      return setTipeAndPrecission(CL.FIX);
   }

   public String toString() {
      return ("FIX");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "DM";
   }
}
