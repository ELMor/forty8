package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class HEX extends CheckGroup {
   public HEX() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 483;
   }

   public Storable getInstance() {
      return new HEX();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.INT_MOD, CL.HEX);
      return false;
   }

   public String toString() {
      return ("HEX");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "IM";
   }
}
