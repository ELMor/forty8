package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class DEC extends CheckGroup {
   public DEC() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 482;
   }

   public Storable getInstance() {
      return new DEC();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.INT_MOD, CL.DEC);
      return false;
   }

   public String toString() {
      return ("DEC");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "IM";
   }
}
