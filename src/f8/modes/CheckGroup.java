/*
 * Created on 09-sep-2003
 *
 
 
 */
package f8.modes;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.IndexRangeException;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.Vector;
import f8.types.Double;
import f8.types.Int;
import f8.ui.CF;
import f8.ui.Menu;
import f8.ui.WithEnter;
import f8.ui.hp48.menu.MODES;

/**
 * @author elinares
 *

 
 */
public abstract class CheckGroup extends NonAlgebraic {
   public abstract String getGroupName();

   public abstract boolean execBefore() throws F8Exception;

   public void exec() throws F8Exception {
      if(execBefore()) {
         return;
      }
      CF     cf=StaticCalcGUI.theGUI.getCalcFisica();
      Vector v=(Vector)Menu.groups.get(getGroupName());
      if(v==null) {
         return;
      }
      int       sz=v.size();
      WithEnter c=null;
      if(sz==1) {
         c=(WithEnter)v.elementAt(0);
         c.setChecked(!c.isChecked());
      } else {
         String search=toString();
         for(int i=0; i<v.size(); i++) {
            c=(WithEnter)v.elementAt(i);
            c.setChecked(c.getEmit().equals(search));
         }
      }

      if(cf.getUpMenu() instanceof MODES) {
         StaticCalcGUI.theGUI.refreshProgMenu();
      }
   }

   protected boolean setTipeAndPrecission(int tipo) throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(1)) {
         Command a=(Command)cl.pop();

         if(a instanceof Int) {
            int n=((Int)a).n;

            if(n>=0) {
               cl.setSettings(CL.DOU_MOD, tipo);
               cl.setSettings(CL.DOU_PRE, n);
               return false;
            } else {
               throw new IndexOutOfBoundsException(null);
            }
         } else if(a instanceof Double) {
            int n=(int)((Double)a).x;

            if(n>=0) {
               cl.setSettings(CL.DOU_MOD, tipo);
               cl.setSettings(CL.DOU_PRE, n);
               return false;
            } else {
               throw new IndexRangeException(this);
            }
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
      return true;
   }
}
