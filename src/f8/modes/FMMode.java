package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class FMMode extends CheckGroup {
   public FMMode() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 479;
   }

   public Storable getInstance() {
      return new FMMode();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL  cl =StaticCalcGUI.theGUI.getCalcLogica();
      int set=((cl.getSetting(CL.PER_MOD)==CL.COMMA) ? CL.PERIOD : CL.COMMA);
      cl.setSettings(CL.PER_MOD, set);
      return false;
   }

   public String toString() {
      return ("0_0");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "FM";
   }
}
