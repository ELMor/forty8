package f8.modes;

import f8.CL;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class GRAD extends CheckGroup {
   public GRAD() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 487;
   }

   public Storable getInstance() {
      return new GRAD();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public boolean execBefore() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.setSettings(CL.ANG_MOD, CL.GRAD);
      return false;
   }

   public String toString() {
      return ("GRAD");
   }

   /* (non-Javadoc)
    * @see f8.kernel.mod.CheckGroup#getGroupName()
    */
   public String getGroupName() {
      return "AM";
   }
}
