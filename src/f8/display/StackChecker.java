/*
 * Created on 14-sep-2003
 *
 
 
 */
package f8.display;

import f8.Stackable;
import f8.StaticCalcGUI;

import f8.kernel.rtExceptions.F8Exception;

import f8.platform.LCDControl;

import f8.types.Complex;
import f8.types.Int;
import f8.types.Lista;

/**
 * @author elinares
 *

 
 */
public class StackChecker {
   public static CSys.Point UserCoordinates(Stackable esto)
                                          throws F8Exception {
      CSys.Point c=null;
      int             x;
      int             y;
      if(esto instanceof Lista) { //Display coo
         Lista list=(Lista)esto;
         if(list.size()==2) {
            if(list.get(0) instanceof Int&&list.get(1) instanceof Int) {
               //Coordenadas fisicas o de pantalla, copiar
               c=CSys.getCoorSystem("lcd").coor(
                                                      ((Int)list.get(0))
                                                      .intValue(),
                                                      ((Int)list.get(1))
                                                      .intValue()
                                                     );
            }
         }
      } else if(esto instanceof Complex) {
         LCDControl lcd=StaticCalcGUI.theGUI.getLCD();
         Complex    cmp=(Complex)esto;
         c=CSys.getCoorSystem("usr").coor(
                                                cmp.complexValue()[0],
                                                cmp.complexValue()[1]
                                               );
      }
      return c;
   }
}
