package f8.display;

import f8.CL;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;

import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;

import f8.platform.DataStream;
import f8.platform.LCDControl;
import f8.platform.Storable;

public final class MARK extends NonAlgebraic {
   public MARK() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 401;
   }

   public Storable getInstance() {
      return new MARK();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      LCDControl lcd=StaticCalcGUI.theGUI.getLCD();
      CL         cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(1)) {
         CSys.Point c=StackChecker.UserCoordinates(cl.peek());
         if(c!=null) {
            cl.pop();
            lcd.setMark(c);
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("MARK");
   }
}
