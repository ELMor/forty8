package f8.display;

import f8.CL;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.LCDControl;
import f8.platform.Storable;

public final class BOX extends NonAlgebraic {
   public BOX() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 403;
   }

   public String toString() {
	  return ("BOX");
   }

   public Storable getInstance() {
      return new BOX();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      LCDControl lcd=StaticCalcGUI.theGUI.getLCD();
      CL         cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(2)) {
         CSys.Point c1=StackChecker.UserCoordinates(cl.peek(1));
         CSys.Point c2=StackChecker.UserCoordinates(cl.peek());
         if(c1!=null && c2!=null) {
         	cl.pop(2);
            lcd.box(c1,c2);;
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

}
