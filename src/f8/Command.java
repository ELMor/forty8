package f8;

import antlr.CommonAST;

import antlr.collections.AST;

import f8.kernel.rtExceptions.F8Exception;

import f8.kernel.yacc.ObjectParserTokenTypes;

import f8.op.keyboard.FLECHA;

import f8.op.prg.brch.DOUNTIL;
import f8.op.prg.brch.FORNEXT;
import f8.op.prg.brch.IF;
import f8.op.prg.brch.IFERR;
import f8.op.prg.brch.WHILEREPEAT;

import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.MathKernel;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.platform.Vector;

import f8.types.Complex;
import f8.types.Double;
import f8.types.F8String;
import f8.types.InfixExp;
import f8.types.Int;
import f8.types.Lista;
import f8.types.Literal;
import f8.types.Matrix;
import f8.types.Proc;
import f8.types.TempUnit;
import f8.types.Unit;

import f8.types.utils.CommandSequence;

public abstract class Command extends Operation implements Storable,
                                                           ObjectParserTokenTypes {
   /**
    * Para contabilizar todos los items
    */
   public static Hashtable cls=UtilFactory.newHashtable();

   /**
    * Crea un array de objetos HP48 hijos de ast
    * @param ast El padre cuyos hijos se van a devolver en un array
    * @return Array de hijos de ast
    */
   public static Vector arrayFromChilds(AST ast) {
      Vector f8oa=UtilFactory.newVector();
      for(ast=ast.getFirstChild(); ast!=null; ast=ast.getNextSibling()) {
         f8oa.add(createFromAST(ast));
      }

      return f8oa;
   }

   /**
    * Usada para crear un objeto HP48 desde un arbol de compilacion antlr
    * @param tree Nodo raiz del arbol de compilacion
    * @return Objeto HP48 que lo representa, o null si no se reconoce.
    */
   public static Command createFromAST(AST def) {
      Command    ret  =null;
      CL         cl   =StaticCalcGUI.theGUI.getCalcLogica();
      MathKernel mk   =UtilFactory.getMath();
      AST        child=def.getFirstChild();

      switch(def.getType()) {
         case TAG:
            String tag=child.getText();
            ret=createFromAST(child.getNextSibling());
            ((Stackable)ret).setTag(tag);
            break;
         case UNIT:
            AST unit=child.getNextSibling();
            if(
               (unit.getNumberOfChildren()==0)
                   &&(
                      (
                         unit.getText().startsWith("�")
                         &&(unit.getText().length()>1)
                      )||unit.getText().equals("K")
                   )
              ) {
               ret=new TempUnit(child);
            } else {
               ret=new Unit(child);
            }
            break;
         case OPERADOR:
            ret=cl.lookup(child.getText());
            if(ret==null) {
               ret=new Literal(child.getText());
            }

            break;
         case ID:
            ret=new Literal(def.getText());
            break;
         case INFIX:
            ret=new InfixExp(def);
            break;
         case INTNUMBER:
            ret=new Int(def);
            break;
         case LITERAL:
            ret=new Literal(child.getText());
            break;
         case LOCALVAR:
            ret=new FLECHA(def);
            break;
         case MATRIZ:
            double[][] v3=
                     new double[def.getNumberOfChildren()][def.getFirstChild()
                                                              .getNumberOfChildren()];
            int        i3 =0;
            int        j3 =0;
            AST        ia3;
            AST        ja3;

            for(ia3=def.getFirstChild(); ia3!=null;
                    ia3=ia3.getNextSibling(), i3++
               ) {
               for(
                   ja3=ia3.getFirstChild(), j3=0; ja3!=null;
                       ja3=ja3.getNextSibling(), j3++
                  ) {
                  v3[i3][j3]=mk.toDouble(ja3.getText());
               }
            }

            ret=new Matrix(v3);

            break;
         case NUMBER:
            ret=new Double(mk.toDouble(def.getText()));
            break;
         case LIST:
            ret=new Lista(arrayFromChilds(def));
            break;
         case PROGRAM:
            ret=new Proc(arrayFromChilds(child));
            break;
         case STRING_LITERAL:
            ret=
                           new F8String(def.getText().substring(
                                                                1,
                                                                def.getText()
                                                                   .length()-1
                                                               )
                                       );
            break;
         case VECTOR:
            double[] v2=new double[def.getNumberOfChildren()];
            int      i2=0;

            for(AST a2=def.getFirstChild(); a2!=null;
                           a2=a2.getNextSibling(), i2++
               ) {
               v2[i2]=mk.toDouble(a2.getText());
            }

            ret=new f8.types.F8Vector(v2);

            break;
         case IF:
         case IFELSE:
            ret=new IF(def);
            break;
         case IFERR:
            ret=new IFERR(def);
            break;
         case FORNEXT:
         case FORSTEP:
            ret=new FORNEXT(def);
            break;
         case DOUNTIL:
            ret=new DOUNTIL(def);
            break;
         case WHILEREPEAT:
            ret=new WHILEREPEAT(def);
            break;
         case SEQOB:
            ret=new CommandSequence(def);
            break;
         case COMPLEX:
            ret=
                        new Complex(
                                    mk.toDouble(child.getText()),
                                    mk.toDouble(child.getNextSibling().getText())
                                   );
            break;
         default:
            ret=cl.lookup(def.getText());
            break;
      }

      return ret;
   }

   /**
    * Para obtener listados de los codigos ID e Items ya definidos
    */

   /*
      public static void listItems()
      {
         for (int i = 0; i < 128; i++) {
            if (classes[i] != null) {
               StkOb iy = (StkOb) classes[i];
               System.out.println(iy.getClass().getName() + "=" + i);
            }
            else {
               System.out.println("");
            }
         }
      }
    */

   /**
    * Comportamiento por defecto de un load desde un DataStream
    * @param ds DataStream desde el que cargar
    * @return una interfaz Storable
    */
   public static Command loadFromStorage(DataStream ds) {
      int id=ds.readInt();

      if(id==0) {
         return null;
      }

      Command obj=(Command)((Storable)cls.get(""+id)).getInstance();
      obj.loadState(ds);

      return obj;
   }

   public static void loadState(DataStream ds, AST[] node) {
      byte check=ds.readByte();
      if(check==(byte)0) {
         node[0]=null;
         return;
      }
      node[0]=new CommonAST();
      node[0].initialize(ds.readInt(), ds.readStringSafe());

      int   numChilds=ds.readInt();

      AST[] child=new CommonAST[1];
      for(int i=0; i<numChilds; i++) {
         loadState(ds, child);
         node[0].addChild(child[0]);
      }
   }

   public static void saveState(DataStream ds, AST node) {
      if(node==null) {
         ds.writeByte((byte)0);
         return;
      } else {
         ds.writeByte((byte)1);
      }
      ds.writeInt(node.getType());
      ds.writeStringSafe(node.getText());
      ds.writeInt(node.getNumberOfChildren());

      for(AST child=node.getFirstChild(); child!=null;
                        child=child.getNextSibling()
         ) {
         saveState(ds, child);
      }
   }

   public static void storeID(Command este) {
      //Ojo aqui. se puede construir con null, pero no nos interesa.
      String index=""+este.getID();
      if(cls.get(index)==null) {
         cls.put(index, este);
      } else {
         StaticCalcGUI.theGUI.log("SYSERR ID:"+este.getID()+este.toString()+","
                                  +cls.get(index).toString()
                                 );
      }
   }

   /**
    * Comportamiento por defecto de exec es un push
    *
    */
   public void exec() throws F8Exception {
      StaticCalcGUI.theGUI.getCalcLogica().push((Stackable)this);
   }

   public abstract int getID();

   public abstract Storable getInstance();

   public abstract boolean isAlgebraic();

   public abstract boolean isInfix();

   public abstract boolean isOperator();

   public abstract void loadState(DataStream ds);

   public abstract void saveState(DataStream ds);

   /**
    * Comportamiento por defecto de un store es un push
    *
    */
   public void store() throws F8Exception {
      Stackable c;
      try {
         c=(Stackable)this;
         StaticCalcGUI.theGUI.getCalcLogica().push(c);
      } catch(ClassCastException cce) {
         this.exec();
      }
   }
}
