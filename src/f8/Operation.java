package f8;

import f8.kernel.rtExceptions.F8Exception;



public abstract class Operation {
   /**
	* Comportamiento por defecto de exec es un push
	*
	*/
   public abstract void exec() throws F8Exception;
   
}
