package f8.types;

import f8.CL;
import f8.Stackable;
import f8.platform.DataStream;
import f8.platform.Storable;

public final class Comment extends Stackable {
   String s;

   public Comment(String s) {
      this.s=s;
   }

   public Stackable copia() {
      return new Comment(new String(s));
   }

   public String getTypeName() {
      return "Comment";
   }

   public int getID() {
      return 3;
   }

   public Storable getInstance() {
      return new Comment("");
   }

   public void loadState(DataStream ds) {
      super.loadState(ds);
      s=ds.readStringSafe();
   }

   public void saveState(DataStream ds) {
      super.saveState(ds);
      ds.writeStringSafe(s);
   }

   public void exec() {
   }

   public void store() {
   }

   public String toString() {
      return ("//"+s);
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkObType#decompose()
    */
   public boolean decompose(CL cl) {
      return false;
   }
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if(obj instanceof Comment){
			return s.equals(((Comment)obj).s);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#sign()
	 */
	public Stackable sign() {
		return new Double(0);
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#zero()
	 */
	public Stackable zero() {
		return zeroComment;
	}
	
	public static Comment zeroComment=new Comment("");

}
