package f8.types.utils;

public interface ComplexValue {
   public double[] complexValue();
}
