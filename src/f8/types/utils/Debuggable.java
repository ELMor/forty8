/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.types.utils;

import f8.kernel.rtExceptions.F8Exception;

/**
 * @author elinares
 *

 
 */
public interface Debuggable {
	/**
	 * Ejecucion de depuracion. Devuelve false si no se desea incrementar
	 * instruction pointer
	 * @return
	 * @throws F8Exception
	 */
	public boolean execDebug(boolean stepInto) throws F8Exception;
	public String nextCommand();
	public boolean isAnyMore();
}
