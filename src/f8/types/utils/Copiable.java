/*
 * Created on 29-ago-2003
 *
 
 
 */
package f8.types.utils;

import f8.Stackable;

/**
 * @author elinares
 *

 
 */
public interface Copiable {
   public Stackable copia();
}
