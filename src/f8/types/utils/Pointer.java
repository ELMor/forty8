package f8.types.utils;

import f8.Command;
import f8.Stackable;

public abstract class Pointer extends Stackable {
   public abstract Command getItem();
}
