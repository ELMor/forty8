package f8.types;

import f8.CL;
import f8.Command;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.IndexRangeException;
import f8.op.prg.obj.TO_LIST;
import f8.platform.DataStream;
import f8.platform.MathKernel;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.types.utils.Array;
import f8.types.utils.DoubleValue;

public final class F8Vector extends Array {
   public double[] x;

   public F8Vector(int d) {
      this.x=new double[d];
   }

   public F8Vector(double[] x) {
      this.x=x;
   }

   public Stackable copia() {
      double[] y=new double[x.length];
      for(int i=0; i<x.length; i++) {
         y[i]=x[i];
      }
      return new F8Vector(y);
   }

   public int getID() {
      return 92;
   }

   public Storable getInstance() {
      return new F8Vector(0);
   }

   public void loadState(DataStream ds) {
      super.loadState(ds);
      int sz=ds.readInt();
      x=new double[sz];

      for(int i=0; i<sz; i++) {
         x[i]=ds.readDouble();
      }
   }

   public void saveState(DataStream ds) {
      super.saveState(ds);
      ds.writeInt(x.length);

      for(int i=0; i<x.length; i++) {
         ds.writeDouble(x[i]);
      }
   }

   public int size() {
      return (x.length);
   }

   // range assumed OK
   public Command get(int i) throws IndexRangeException {
   	try{
			return (new Double(x[i]));
   	}catch(Exception e){
   		throw new IndexRangeException(this);
   	}
      
   }

   public void put(Command a, int i) throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(a instanceof DoubleValue) {
         if((0<=i)&&(i<x.length)) {
            double y=((DoubleValue)a).doubleValue();
            x[i]=y;
         } else {
            throw new IndexRangeException(this);
         }
      } else {
         throw new BadArgumentTypeException(this);
      }
   }

   public String toString() {
      StringBuffer s   =new StringBuffer("[");
      CL           cl  =StaticCalcGUI.theGUI.getCalcLogica();
      MathKernel   mk  =UtilFactory.getMath();
      int          dmod=cl.getSetting(CL.DOU_MOD);
      int          dpre=cl.getSetting(CL.DOU_PRE);

      for(int i=0; i<x.length; i++) {
         s.append(mk.format(x[i], dmod, dpre));
         if(i<x.length-1)
         	s.append(' ');
      }
      
      if(x.length==0)
      	s.append("<empty vector>");

      s.append(']');

      String ret=getTag();
      if(!ret.equals("")) {
         ret+=":";
      }
      return ret+s.toString();
   }

   public String getTypeName() {
      return "F8Vector";
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
    */
   public boolean decompose(CL cl) throws F8Exception {
      for(int i=0; i<x.length; i++) {
         cl.push(new Double(x[i]));
      }
      cl.push(new Double(x.length));
      cl.push(new Double(1));
      new TO_LIST().exec();
      return false;
   }
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if(obj instanceof F8Vector){
			F8Vector cobj=(F8Vector)obj;
			if(cobj.x.length!=x.length){
				return false;
			}
			for(int i=0;i<x.length;i++){
				if(x[i]!=cobj.x[i])
					return false;
			}
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		if(obj instanceof F8Vector){
			F8Vector cobj=(F8Vector)obj;
			if(cobj.x.length!=x.length){
				return false;
			}
			double sumTol=0;
			for(int i=0;i<x.length;i++){
				sumTol+=Math.abs(x[i]-cobj.x[i]);
			}
			return sumTol<tol;
		}
		return false;
	}

}
