/*
 * Created on 16-sep-2003
 *
 
 
 */
package f8.types;

import antlr.collections.AST;
import f8.CL;
import f8.Stackable;
import f8.op.keyboard.mkl;
import f8.platform.Storable;

/**
 * @author elinares
 *

 
 */
public class TempUnit extends Unit {
   /**
    * @param v
    * @param u
    */
   public TempUnit(double v, AST u) {
      super(v, u);
   }

   /**
    * @param def
    */
   public TempUnit(AST def) {
      super(def);
   }

   /**
    * @param value
    * @param unit
    */
   public TempUnit(String value, AST unit) {
      super(value, unit);
   }

   public boolean convert(CL cl, Unit to) {
      String   fromS=getUnidad().getText();
      String   toS=to.getUnidad().getText();
      TempUnit aux=(TempUnit)ubase();
      if(toS.equals("�C")) {
         cl.push(aux.toC());
         return false;
      }
      if(toS.equals("�R")) {
         cl.push(aux.toR());
         return false;
      }
      if(toS.equals("�F")) {
         cl.push(aux.toF());
         return false;
      }
      cl.push(aux);
      return false;
   }

   /* (non-Javadoc)
    * @see f8.kernel.types.Unit#ubase()
    */
   public Unit ubase() {
      String nom=getUnidad().getText();
      AST    K=mkl.fca("K", null);
      if(nom.equals("�C")) {
         return new TempUnit(273.15+getValor(), K);
      }
      if(nom.equals("�F")) {
         return new TempUnit(255.3722222222+((5*getValor())/9), K);
      }
      if(nom.equals("�R")) {
         return new TempUnit((5*getValor())/9, K);
      }
      return this;
   }

   public TempUnit toC() {
      return new TempUnit(getValor()-273.15, mkl.fca("�C", null));
   }

   public TempUnit toF() {
      return new TempUnit(
                          ((-255.372222222+getValor())*9)/5, mkl.fca(
                                                                     "�F", null
                                                                    )
                         );
   }

   public TempUnit toR() {
      return new TempUnit((getValor()*9)/5, mkl.fca("�R", null));
   }

   /* (non-Javadoc)
    * @see f8.kernel.types.utils.Copiable#copia()
    */
   public Stackable copia() {
      return new TempUnit(getValor(), mkl.copyAST(getUnidad()));
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getID()
    */
   public int getID() {
      return 430;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getInstance()
    */
   public Storable getInstance() {
      return new TempUnit(null);
   }

   /* (non-Javadoc)
    * @see f8.kernel.StackableObject#getTypeName()
    */
   public String getTypeName() {
      return "TempUnit";
   }
}
