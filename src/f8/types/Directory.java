/*
 * Created on 06-ago-2003
 *
 
 
 */
package f8.types;

import f8.CL;
import f8.Command;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.Storable;
import f8.platform.UtilFactory;
import f8.platform.Vector;
import f8.ui.CF;
import f8.ui.VAR;

/**
 * @author elinares
 *

 
 */
public final class Directory extends Stackable {
   Hashtable ht    =UtilFactory.newHashtable();
   String    nombre;

   public Directory(Directory parent, String name) {
      if(name==null) {
         return;
      }
      if(name.startsWith("'")) {
         name=name.substring(1);
      }

      if(name.endsWith("'")) {
         name=name.substring(0, name.length()-1);
      }

      nombre=name;

      if(!nombre.equals("System")&&!nombre.equals("")) {
         parent.put(nombre, this);
      }
   }

   public Stackable copia() {
      return null; //Los directorios no los quiero copiar 				
   }

   public String getTypeName() {
      return "Directory";
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getID()
    */
   public int getID() {
      return 127;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getInstance()
    */
   public Storable getInstance() {
      return new Directory(null, "");
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
    */
   public void saveState(DataStream ds) {
      super.saveState(ds);
      ds.writeStringSafe(nombre);

      Vector k=ht.getKeys();
      ds.writeInt(k.size());

      for(int i=0; i<k.size(); i++) {
         Storable item=(Storable)ht.get(k.elementAt(i));
         ds.writeStringSafe((String)k.elementAt(i));
         ds.writeInt(item.getID());
         item.saveState(ds);
      }
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
    */
   public void loadState(DataStream ds) {
      super.loadState(ds);
      nombre=ds.readStringSafe();

      int sz=ds.readInt();

      for(int i=0; i<sz; i++) {
         String   n  =ds.readStringSafe();
         Storable obj=Command.loadFromStorage(ds);
         ht.put(n, obj);
      }
   }

   public Hashtable getHT() {
      return ht;
   }

   public String getNombre() {
      return nombre;
   }

   public void put(String name, Object obj) {
      ht.put(name, obj);
   }

   public Command get(String name) {
      return (Command)ht.get(name);
   }

   public void exec() {
      StaticCalcGUI.theGUI.getCalcLogica().changeToDir(nombre);

      CF cf=StaticCalcGUI.theGUI.getCalcFisica();

      if(cf.getUpMenu() instanceof VAR) {
         StaticCalcGUI.theGUI.setMenu(1, new VAR(), null, null);
      }
   }

   public String toString() {
      String ret="DIR ";
      Vector v=ht.getKeys();
      for(int i=0; i<v.size(); i++) {
         ret+=(
                  " "+(String)v.elementAt(i)+" "
                  +((Command)ht.get(v.elementAt(i))).toString()
               );
      }
      return ret+" END";
   }

   /* (non-Javadoc)
    * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
    */
   public boolean decompose(CL cl) {
      return true;
   }
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#sign()
	 */
	public Stackable sign() {
		return new Double(0);
	}

	/* (non-Javadoc)
	 * @see f8.Stackable#zero()
	 */
	public Stackable zero() {
		return new Directory(null,null);
	}

}
