package f8.constants;

import f8.CL;
import f8.Stackable;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.utils.Const;

public final class E extends Const {
   public E() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 2379;
   }

   public Storable getInstance() {
      return new E();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable value() {
      return new Double(Math.E);
   }

   public String toString() {
      return ("e");
   }
	/* (non-Javadoc)
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		return false;
	}

}
