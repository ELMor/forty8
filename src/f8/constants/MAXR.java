package f8.constants;

import f8.CL;
import f8.Stackable;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Double;
import f8.types.utils.Const;

public final class MAXR extends Const {
   public MAXR() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 2380;
   }

   public Storable getInstance() {
      return new MAXR();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public Stackable value() {
      return new Double(9.999999999999E307);
   }

   public String toString() {
      return ("MAXR");
   }
	/* (non-Javadoc)
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose(CL cl) {
		return false;
	}

}
