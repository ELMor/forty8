package f8;

import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.op.keyboard.mkl;
import f8.types.Complex;
import f8.types.Double;
import f8.types.F8String;
import f8.types.F8Vector;
import f8.types.InfixExp;
import f8.types.Int;
import f8.types.Lista;
import f8.types.Literal;
import f8.types.Matrix;
import f8.types.Unit;
import f8.types.utils.CommandSequence;

public abstract class Dispatch1 extends Command {
   public final void exec() throws F8Exception {
      CL              cl =StaticCalcGUI.theGUI.getCalcLogica();
      Stackable res=null;

      if(cl.check(1)) {
         Stackable a=cl.peek();
         if(a instanceof Double) {
            res=prfDouble((Double)a);
         } else if(a instanceof Unit) {
            res=prfUnit((Unit)a);
         } else if(a instanceof InfixExp||a instanceof Literal) {
            res=prfInfix((InfixExp)a);
         } else if(a instanceof Complex) {
            res=prfComplex((Complex)a);
         } else if(a instanceof F8Vector) {
            res=prfVector((F8Vector)a);
         } else if(a instanceof Matrix) {
            res=prfMatrix((Matrix)a);
         } else if(a instanceof Lista) {
            res=prfLista((Lista)a);
         }else if(a instanceof F8String){
         	res=prfString((F8String)a);
         } else if(a instanceof Int){
         	res=prfInt((Int)a);
         } else {
            throw new BadArgumentTypeException(this);
         }
         if(res!=null) {
            cl.pop();
            if(res instanceof CommandSequence){
            	CommandSequence csres=(CommandSequence)res;
            	for(int i=0;i<csres.size();i++)
            		cl.push((Stackable)csres.get(i));
            }else{
					cl.push(res);
            }
            
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public Stackable prfDouble(Double x) throws F8Exception {
      return null;
   }

   public Stackable prfUnit(Unit x) throws F8Exception {
      return null;
   }

   public Stackable prfComplex(Complex x) throws F8Exception {
      return null;
   }

   public Stackable prfVector(F8Vector x) throws F8Exception {
      return null;
   }

   public Stackable prfMatrix(Matrix x) throws F8Exception {
      return null;
   }

   public Stackable prfLista(Lista x) throws F8Exception {
	  return null;
   }

   public Stackable prfString(F8String x) throws F8Exception {
	  return null;
   }

   public Stackable prfInt(Int x) throws F8Exception {
	  return null;
   }

   public Stackable prfInfix(InfixExp a) throws F8Exception {
      return new InfixExp(mkl.fca(toString(), a.getAST()));
   }

   public  String undo(){
   	return null;
   }
   
	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
