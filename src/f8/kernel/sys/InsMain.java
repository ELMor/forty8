package f8.kernel.sys;

import antlr.collections.AST;

import f8.CL;
import f8.Command;

import f8.constants.E;
import f8.constants.I;
import f8.constants.MAXR;
import f8.constants.MINR;
import f8.constants.PI;

import f8.display.AUTO;
import f8.display.BOX;
import f8.display.CIRCLE;
import f8.display.DRAW;
import f8.display.ERASE;
import f8.display.LABEL;
import f8.display.LINE;
import f8.display.MARK;
import f8.display.XRNG;
import f8.display.YRNG;

import f8.kernel.op.arr.DIM;
import f8.kernel.op.arr.Gauss;
import f8.kernel.op.arr.JORDAN;
import f8.kernel.op.arr.MkMatrix;
import f8.kernel.op.arr.MkVector;
import f8.kernel.op.arr.RowScale;
import f8.kernel.op.arr.RowSub;
import f8.kernel.op.arr.RowSwap;

import f8.modes.BIN;
import f8.modes.ChkARG;
import f8.modes.ChkBEEP;
import f8.modes.ChkCLK;
import f8.modes.ChkCMD;
import f8.modes.ChkCNCT;
import f8.modes.ChkML;
import f8.modes.ChkSTK;
import f8.modes.ChkSYM;
import f8.modes.D3Mode1;
import f8.modes.D3Mode2;
import f8.modes.D3Mode3;
import f8.modes.DEC;
import f8.modes.DEG;
import f8.modes.ENG;
import f8.modes.FIX;
import f8.modes.FMMode;
import f8.modes.GRAD;
import f8.modes.HEX;
import f8.modes.OCT;
import f8.modes.RAD;
import f8.modes.SCI;
import f8.modes.STD;

import f8.op.keyboard.ACOS;
import f8.op.keyboard.ALOG;
import f8.op.keyboard.ASIN;
import f8.op.keyboard.ASSIGN;
import f8.op.keyboard.ATAN;
import f8.op.keyboard.CARET;
import f8.op.keyboard.CHS;
import f8.op.keyboard.COS;
import f8.op.keyboard.DER;
import f8.op.keyboard.DIV;
import f8.op.keyboard.EQ;
import f8.op.keyboard.EXP;
import f8.op.keyboard.FLECHA;
import f8.op.keyboard.GE;
import f8.op.keyboard.GT;
import f8.op.keyboard.INV;
import f8.op.keyboard.Integral;
import f8.op.keyboard.LE;
import f8.op.keyboard.LN;
import f8.op.keyboard.LOG;
import f8.op.keyboard.LT;
import f8.op.keyboard.MINUS;
import f8.op.keyboard.PLUS;
import f8.op.keyboard.SIN;
import f8.op.keyboard.SQ;
import f8.op.keyboard.SQRT;
import f8.op.keyboard.Sigma;
import f8.op.keyboard.TAN;
import f8.op.keyboard.TIMES;
import f8.op.keyboard.mkl;

import f8.op.math.base.AND;
import f8.op.math.base.NOT;
import f8.op.math.base.OR;
import f8.op.math.base.XOR;
import f8.op.math.hyp.ACOSH;
import f8.op.math.hyp.ASINH;
import f8.op.math.hyp.ATANH;
import f8.op.math.hyp.COSH;
import f8.op.math.hyp.EXPM;
import f8.op.math.hyp.LNP1;
import f8.op.math.hyp.SINH;
import f8.op.math.hyp.TANH;
import f8.op.math.matrx.DET;
import f8.op.math.parts.ABS;
import f8.op.math.parts.ARG;
import f8.op.math.parts.CEIL;
import f8.op.math.parts.CONJ;
import f8.op.math.parts.FLOOR;
import f8.op.math.parts.FP;
import f8.op.math.parts.IM;
import f8.op.math.parts.IP;
import f8.op.math.parts.MANT;
import f8.op.math.parts.MAX;
import f8.op.math.parts.MIN;
import f8.op.math.parts.MOD;
import f8.op.math.parts.PCT;
import f8.op.math.parts.PCTCH;
import f8.op.math.parts.PCTT;
import f8.op.math.parts.RE;
import f8.op.math.parts.RND;
import f8.op.math.parts.SIGN;
import f8.op.math.parts.TRNC;
import f8.op.math.parts.XPON;
import f8.op.math.prob.COMB;
import f8.op.math.prob.FACTORIAL;
import f8.op.math.prob.PERM;
import f8.op.math.vectr.CROSS;
import f8.op.math.vectr.DFLECHAR;
import f8.op.math.vectr.FLECHAV2;
import f8.op.math.vectr.FLECHAV3;
import f8.op.math.vectr.RFLECHAD;
import f8.op.math.vectr.VFLECHA;

import f8.op.prg.brch.DOUNTIL;
import f8.op.prg.brch.FORNEXT;
import f8.op.prg.brch.IF;
import f8.op.prg.brch.STOP;
import f8.op.prg.brch.WHILEREPEAT;
import f8.op.prg.ctrl.HALT;
import f8.op.prg.obj.C_TO_R;
import f8.op.prg.obj.DTAG;
import f8.op.prg.obj.EQ_TO;
import f8.op.prg.obj.OBJ_TO;
import f8.op.prg.obj.R_TO_C;
import f8.op.prg.obj.TO_ARRY;
import f8.op.prg.obj.TO_LIST;
import f8.op.prg.obj.TO_STR;
import f8.op.prg.obj.TO_TAG;
import f8.op.prg.obj.TO_UNIT;
import f8.op.prg.obj.TYPE;
import f8.op.prg.obj.VTYPE;
import f8.op.prg.stk.COPY;
import f8.op.prg.stk.DEPTH;
import f8.op.prg.stk.DROP;
import f8.op.prg.stk.DROP2;
import f8.op.prg.stk.DROPN;
import f8.op.prg.stk.DUP;
import f8.op.prg.stk.DUP2;
import f8.op.prg.stk.DUPN;
import f8.op.prg.stk.EVAL;
import f8.op.prg.stk.OVER;
import f8.op.prg.stk.PICK;
import f8.op.prg.stk.ROLL;
import f8.op.prg.stk.ROLLD;
import f8.op.prg.stk.ROT;
import f8.op.prg.stk.SWAP;

import f8.op.units.CONVERT;
import f8.op.units.UBASE;
import f8.op.units.UFACT;
import f8.op.units.UVAL;

import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.UtilFactory;

import f8.storage.CRDIR;
import f8.storage.HOME;
import f8.storage.MENU;
import f8.storage.PGDIR;
import f8.storage.PURGE;
import f8.storage.STEQ;
import f8.storage.STO;
import f8.storage.UPDIR;

import f8.symbolic.COLECT;
import f8.symbolic.EXPAND;

import f8.types.Comment;
import f8.types.Complex;
import f8.types.Directory;
import f8.types.Double;
import f8.types.F8String;
import f8.types.F8Vector;
import f8.types.InfixExp;
import f8.types.Int;
import f8.types.Lista;
import f8.types.Literal;
import f8.types.Matrix;
import f8.types.Proc;
import f8.types.TempUnit;
import f8.types.Unit;

import f8.types.utils.CommandSequence;

public final class InsMain {
   public static void installStkOb(CL cl) {
      Command x;

      x=new CHS();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new STO();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new ASSIGN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new EVAL();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DOUNTIL(null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new IF(null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new NOT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new AND();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new OR();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DUP();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new COPY();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DROP();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new SWAP();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new FLOOR();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new STOP();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new WHILEREPEAT(null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new Int(1);
      cl.systemInstall(x);
      Command.storeID(x);
      x=new Literal(null);
      cl.systemInstall(x);
      Command.storeID(x);
      x=new Comment(null);
      cl.systemInstall(x);
      Command.storeID(x);
      x=new FIX();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new CommandSequence((AST)null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new ASIN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new ATAN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new TAN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new Proc(null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new SCI();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new F8String(null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new SQRT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new COS();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new SIN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new ACOS();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new PI();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new LOG();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new EXP();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new CARET();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new Double(Math.PI);
      cl.systemInstall(x);
      Command.storeID(x);
      x=new PLUS();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new MINUS();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new TIMES();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DIV();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new CROSS();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DIM();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new I();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new Gauss();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new JORDAN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new MkMatrix();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new MkVector();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DET();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new GE();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new GT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new LE();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new LT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new EQ();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new ABS();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new RE();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new IM();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new RowSwap();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new RowSub();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new RowScale();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new Complex(0, 0);
      cl.systemInstall(x);
      Command.storeID(x);

      x=new Matrix(new double[][]{
                      {1}
                   }
                  );
      cl.systemInstall(x);
      Command.storeID(x);
      x=new CRDIR();
      cl.systemInstall(x);
      Command.storeID(x); //125
      x=new HOME();
      //cl.systemInstall(x);
      Command.storeID(x); //124
      x=new UPDIR();
      cl.systemInstall(x);
      Command.storeID(x); //123
      x=new InfixExp(null);
      //cl.systemInstall(x); 
      Command.storeID(x); //119
      x=new Unit(null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new FORNEXT(null);
      //cl.systemInstall(x); 
      Command.storeID(x);
      x=new DER();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new LN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new UBASE();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new UFACT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new COLECT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new CONVERT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new INV();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new EXPAND();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new OVER();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new ROLL();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new ROLLD();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new ROT();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new PICK();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new DEPTH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new DUP2();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DUPN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DROP2();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new DROPN();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new SQ();
      cl.systemInstall(x);
      Command.storeID(x);
      x=new PURGE();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new Directory(null, null);
      Command.storeID(x);

      x=new UVAL();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TO_UNIT();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ENG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new STD();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkBEEP();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkSYM();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkSTK();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkARG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkCMD();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkCNCT();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkCLK();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new DEG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new RAD();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new GRAD();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new D3Mode1();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new D3Mode2();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new D3Mode3();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new HEX();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new DEC();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new OCT();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new BIN();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new FMMode();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ChkML();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new MARK();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new LINE();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new BOX();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new CIRCLE();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TempUnit(null);
      Command.storeID(x);

      x=new MENU();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TO_TAG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new PGDIR();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new Sigma(null);
      cl.systemInstall(x);
      Command.storeID(x);

      x=new Integral(null);
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ALOG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new XOR();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new SIGN();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new CONJ();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ARG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new MIN();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new MAX();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new MOD();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new PCT();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new PCTT();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new PCTCH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new MANT();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new XPON();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new FP();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new IP();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new CEIL();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new RND();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TRNC();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new E();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new MAXR();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new MINR();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new COMB();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new FACTORIAL();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new PERM();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new SINH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new COSH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TANH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ASINH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ACOSH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ATANH();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new EXPM();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new LNP1();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new HALT();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new FLECHA(null);
      cl.systemInstall(x);
      Command.storeID(x);

      x=new STEQ();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new DRAW();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new Lista(UtilFactory.newVector());
      cl.systemInstall(x);
      Command.storeID(x);

      x=new ERASE();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new AUTO();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new XRNG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new LABEL();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new YRNG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new F8Vector(new double[0]);
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TO_LIST();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new C_TO_R();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new DTAG();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new EQ_TO();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new OBJ_TO();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new R_TO_C();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TO_ARRY();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TO_STR();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new TYPE();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new VTYPE();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new VFLECHA();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new RFLECHAD();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new FLECHAV3();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new FLECHAV2();
      cl.systemInstall(x);
      Command.storeID(x);

      x=new DFLECHAR();
      cl.systemInstall(x);
      Command.storeID(x);
   }

   public static void installUnits(
                                   boolean existFile, DataStream ds, Hashtable u
                                  ) {
      if(existFile) {
         ds.setRecordPos(0);
         int    sz  =ds.readInt();
         Unit   unit;
         String name;
         for(int i=0; i<sz; i++) {
            name   =ds.readStringSafe();
            unit   =new Unit(null);
            unit.loadState(ds);
            u.put(name, unit);
         }
      } else {
         AST metro  =mkl.fca("m", null);
         AST sqMeter=mkl.ele(metro, mkl.num(2));
         AST cuMeter=mkl.ele(metro, mkl.num(3));
         AST segun  =mkl.fca("s", null);
         AST mps    =mkl.div(metro, segun);
         AST mps2   =mkl.div(mps, segun);
         AST kg     =mkl.fca("kg", null);
         AST mol    =mkl.fca("mol", null);
         AST N      =mkl.mul(kg, mkl.div(metro, mkl.ele(segun, mkl.num(2))));
         AST J      =mkl.mul(N, metro);
         AST W      =mkl.div(J, segun);
         AST Pa     =mkl.div(kg, mkl.mul(metro, mkl.ele(segun, mkl.num(2))));
         AST A      =mkl.fca("A", null);
         AST cd     =mkl.fca("cd", null);
         AST cdpm2  =mkl.div(cd, mkl.ele(metro, mkl.num(2)));
         AST m2ps2  =
                     mkl.mul(
                             mkl.ele(metro, mkl.num(2)),
                             mkl.ele(segun, mkl.num(2))
                            );
         AST gK    =mkl.fca("�K", null);
         AST angulo=mkl.fca("angulo", null);
         AST arcos =mkl.fca("arcos", null);

         u.put("m", new Unit(null));
         u.put("cm", new Unit(0.01, metro));
         u.put("mm", new Unit(0.001, metro));
         u.put("yd", new Unit(0.9144, metro));
         u.put("ft", new Unit(0.3048, metro));
         u.put("in", new Unit(0.0254, metro));
         u.put("Mpc", new Unit(3.08567818585E22, metro));
         u.put("pc", new Unit(3.08567818585E16, metro));
         u.put("lyr", new Unit(9.46052840488E15, metro));
         u.put("au", new Unit(14959790E4, metro));
         u.put("km", new Unit(1000, metro));
         u.put("mi", new Unit(1609.344, metro));
         u.put("nmi", new Unit(1852, metro));
         u.put("miUS", new Unit(1609.34721869, metro));
         u.put("chain", new Unit(20.1168402337, metro));
         u.put("rd", new Unit(5.02921005842, metro));
         u.put("fath", new Unit(1.82880365761, metro));
         u.put("ftUS", new Unit(0.304800609601, metro));
         u.put("mil", new Unit(0.0000254, metro));
         u.put("micro", new Unit(0.000001, metro));
         u.put("argst", new Unit(0.0000000001, metro));
         u.put("fermi", new Unit(1E-15, metro));

         u.put("b", new Unit(1E-28, sqMeter));
         u.put("ha", new Unit(10000, sqMeter));
         u.put("a", new Unit(100, sqMeter));
         u.put("acre", new Unit(4046.87260987, sqMeter));

         u.put("fbm", new Unit(0.002359737216, cuMeter));
         u.put("pk", new Unit(0.0088097675, cuMeter));
         u.put("bu", new Unit(0.03523907, cuMeter));
         u.put("bbl", new Unit(0.158987294928, cuMeter));
         u.put("tsp", new Unit(4.92892159375E-6, cuMeter));
         u.put("tbsp", new Unit(1.47867647813E-5, cuMeter));
         u.put("ozUK", new Unit(0.000028413075, cuMeter));
         u.put("ozfl", new Unit(2.95735295625E-5, cuMeter));
         u.put("cu", new Unit(2.365882365E-4, cuMeter));
         u.put("ml", new Unit(0.000001, cuMeter));
         u.put("pt", new Unit(0.000473176473, cuMeter));
         u.put("qt", new Unit(0.000946352946, cuMeter));
         u.put("gal", new Unit(0.003785411784, cuMeter));
         u.put("galC", new Unit(0.00454609, cuMeter));
         u.put("galUK", new Unit(0.004546092, cuMeter));
         u.put("l", new Unit(0.001, cuMeter));
         u.put("st", new Unit(1.0, cuMeter));

         u.put("s", new Unit(null));
         u.put("Hz", new Unit(1, mkl.div(mkl.num(1), segun)));
         u.put("h", new Unit(3600, segun));
         u.put("min", new Unit(60, segun));
         u.put("d", new Unit(86400, segun));
         u.put("yr", new Unit(31556925.9747, segun));

         u.put("ga", new Unit(9.80665, mps2));
         u.put("c", new Unit(299792458, mps));
         u.put("knot", new Unit(0.514444444444, mps));
         u.put("mph", new Unit(0.44704, mps));
         u.put("kph", new Unit(0.277777777778, mps));

         u.put("kg", new Unit(1, kg));
         u.put("g", new Unit(0.001, kg));
         u.put("lb", new Unit(0.45359237, kg));
         u.put("mol", new Unit(1, mol));
         u.put("u", new Unit(1.66057E-27, kg));
         u.put("grain", new Unit(0.06479891E-3, kg));
         u.put("ct", new Unit(0.2E-3, kg));
         u.put("ozt", new Unit(31.103475E-3, kg));
         u.put("t", new Unit(1000000E-3, kg));
         u.put("tonUK", new Unit(1016046.9088E-3, kg));
         u.put("ton", new Unit(907184.74E-3, kg));
         u.put("lbt", new Unit(373.2417E-3, kg));
         u.put("slug", new Unit(14593.9029372E-3, kg));
         u.put("oz", new Unit(28.349523125E-3, kg));

         u.put("N", new Unit(1, N));
         u.put("pdl", new Unit(138.254954376E-3, N));
         u.put("lbf", new Unit(4448.22161526E-3, N));
         u.put("kip", new Unit(4448221.61526E-3, N));
         u.put("gf", new Unit(9.80665E-3, N));
         u.put("dyn", new Unit(0.01E-3, N));

         u.put("J", new Unit(1, J));
         u.put("MeV", new Unit(1.60219E-13, J));
         u.put("eV", new Unit(1.60219E-19, J));
         u.put("therm", new Unit(105506000, J));
         u.put("Btu", new Unit(1055.05585262, J));
         u.put("cal", new Unit(4.1868, J));
         u.put("kcal", new Unit(4186.8, J));
         u.put("erg", new Unit(0.0000001, J));

         u.put("W", new Unit(1, W));
         u.put("hp", new Unit(745.699871582, W));

         u.put("Pa", new Unit(1, Pa));
         u.put("inH20", new Unit(248.84, Pa));
         u.put("inHg", new Unit(3386.38815789, Pa));
         u.put("mmHg", new Unit(133.322368421, Pa));
         u.put("torr", new Unit(133.322368421, Pa));
         u.put("psi", new Unit(6894.75729317, Pa));
         u.put("bar", new Unit(100000, Pa));
         u.put("atm", new Unit(101325, Pa));

         u.put("A", new Unit(1, A));
         u.put("C", new Unit(1, mkl.mul(A, segun)));
         u.put(
               "V",
               new Unit(
                        1,
                        mkl.mul(
                                kg,
                                mkl.div(
                                        mkl.ele(metro, mkl.num(2)),
                                        mkl.mul(A, mkl.ele(segun, mkl.num(3)))
                                       )
                               )
                       )
              );
         u.put(
               "F",
               new Unit(
                        1,
                        mkl.mul(
                                mkl.ele(A, mkl.num(2)),
                                mkl.div(
                                        mkl.ele(segun, mkl.num(4)),
                                        mkl.mul(kg, mkl.ele(metro, mkl.num(2)))
                                       )
                               )
                       )
              );
         u.put(
               "Ohm",
               new Unit(
                        1,
                        mkl.mul(
                                kg,
                                mkl.div(
                                        mkl.ele(metro, mkl.num(2)),
                                        mkl.mul(
                                                mkl.ele(A, mkl.num(2)),
                                                mkl.ele(segun, mkl.num(3))
                                               )
                                       )
                               )
                       )
              );
         u.put(
               "Wb",
               new Unit(
                        1,
                        mkl.mul(
                                kg,
                                mkl.div(
                                        mkl.ele(metro, mkl.num(2)),
                                        mkl.mul(A, mkl.ele(segun, mkl.num(2)))
                                       )
                               )
                       )
              );
         u.put(
               "T",
               new Unit(1, mkl.div(kg, mkl.mul(A, mkl.ele(segun, mkl.num(2)))))
              );
         u.put(
               "S",
               new Unit(
                        1,
                        mkl.mul(
                                mkl.ele(A, mkl.num(2)),
                                mkl.div(
                                        mkl.ele(segun, mkl.num(3)),
                                        mkl.mul(kg, mkl.ele(metro, mkl.num(2)))
                                       )
                               )
                       )
              );
         u.put(
               "mho",
               new Unit(
                        1,
                        mkl.mul(
                                mkl.ele(A, mkl.num(2)),
                                mkl.div(
                                        mkl.ele(segun, mkl.num(3)),
                                        mkl.mul(kg, mkl.ele(metro, mkl.num(2)))
                                       )
                               )
                       )
              );
         u.put(
               "H",
               new Unit(
                        1,
                        mkl.mul(
                                kg,
                                mkl.div(
                                        mkl.ele(metro, mkl.num(2)),
                                        mkl.mul(
                                                mkl.ele(A, mkl.num(2)),
                                                mkl.ele(segun, mkl.num(2))
                                               )
                                       )
                               )
                       )
              );
         u.put("Fdy", new Unit(96487, mkl.mul(A, segun)));

         u.put("cd", new Unit(1, cd));
         u.put("lam", new Unit(3183.09886184, cdpm2));
         u.put("lm", new Unit(7.957747154559E-2, cd));
         u.put("sb", new Unit(10000, cdpm2));
         u.put("ph", new Unit(795.774715459, cdpm2));
         u.put("lx", new Unit(7.95774715459E-2, cdpm2));
         u.put("flam", new Unit(3.42625909964, cdpm2));
         u.put("fc", new Unit(0.856564774909, cdpm2));

         u.put("R", new Unit(0.000258, mkl.mul(A, mkl.div(segun, kg))));
         u.put("Ci", new Unit(37E9, mkl.inv(segun)));
         u.put("Bq", new Unit(1, mkl.inv(segun)));
         u.put("Sv", new Unit(1, m2ps2));
         u.put("rem", new Unit(0.01, m2ps2));
         u.put("rad", new Unit(0.01, m2ps2));
         u.put("Gy", new Unit(1, m2ps2));

         u.put("P", new Unit(0.1, mkl.div(kg, mkl.mul(metro, segun))));
         u.put(
               "St",
               new Unit(0.0001, mkl.div(mkl.ele(metro, mkl.num(2)), segun))
              );

         u.put("K", new Unit(null));
         u.put("�C", new Unit(1, gK));
         u.put("�F", new Unit(5/9, gK));
         u.put("�R", new Unit(5/9, gK));

         u.put("r", new Unit(1.0/2.0/3.14159265359, angulo));
         u.put("�", new Unit(1.0/360.0, angulo));
         u.put("grad", new Unit(1.0/400.0, angulo));

         u.put("arcs", new Unit(1.0/360.0/36.0/100, arcos));
         u.put("arcmin", new Unit(1.0/6.0/36.0/100, arcos));
         u.put("sr", new Unit(1031.32403124/360/36, arcos));
      }
   }
}
