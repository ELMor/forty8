/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.kernel.rtExceptions;

import f8.Command;
import f8.ui.TeclaVirtual;

;

/**
 * @author elinares
 *

 
 */
public class WrongNumberOfArgumentsException extends f8.kernel.rtExceptions.F8Exception {

	/**
	 * 
	 */
	public WrongNumberOfArgumentsException(Command c) {
		super(c );
		
	}

	/**
	 * @param message
	 */
	public WrongNumberOfArgumentsException(String message) {
		super(message);
		
	}

	/**
	 * @param v
	 */
	public WrongNumberOfArgumentsException(TeclaVirtual v) {
		super(v);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.WrongNumberOfArg;
	}

}
