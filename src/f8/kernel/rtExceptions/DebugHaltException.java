/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.kernel.rtExceptions;

import f8.Command;
import f8.ui.TeclaVirtual;

;

/**
 * @author elinares
 *

 
 */
public class DebugHaltException extends f8.kernel.rtExceptions.F8Exception {
	/**
	 * 
	 */
	public DebugHaltException(Command c) {
		super(c);
	}

	/**
	 * @param message
	 */
	public DebugHaltException(String message) {
		super(message);
	}

	/**
	 * @param v
	 */
	public DebugHaltException(TeclaVirtual v) {
		super(v);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		
		return ICalcErr.BadArgumentType;
	}

}
