/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.kernel.rtExceptions;

import f8.Command;

;

/**
 * @author elinares
 *

 
 */
public class DivideByZeroException extends f8.kernel.rtExceptions.F8Exception {

	/**
	 * 
	 */
	public DivideByZeroException(Command c) {
		super(c);
		
	}

	/**
	 * @param message
	 */
	public DivideByZeroException(String message) {
		super(message);
		
	}

	/* (non-Javadoc)
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.ZeroDiv;
	}

}
