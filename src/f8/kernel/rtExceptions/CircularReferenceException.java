/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.kernel.rtExceptions;

import f8.Command;

;

/**
 * @author elinares
 *

 
 */
public class CircularReferenceException extends f8.kernel.rtExceptions.F8Exception {

	/**
	 * 
	 */
	public CircularReferenceException(Command c) {
		super(c);
	}

	/**
	 * @param message
	 */
	public CircularReferenceException(String message) {
		super(message);
		
	}

	/* (non-Javadoc)
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.CircularReference;
	}

}
