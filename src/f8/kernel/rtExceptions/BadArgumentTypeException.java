/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.kernel.rtExceptions;

import f8.Command;
import f8.ui.TeclaVirtual;

;

/**
 * @author elinares
 *

 
 */
public class BadArgumentTypeException extends f8.kernel.rtExceptions.F8Exception {
	/**
	 * 
	 */
	public BadArgumentTypeException(Command c) {
		super(c);
	}

	/**
	 * @param message
	 */
	public BadArgumentTypeException(String message) {
		super(message);
	}

	/**
	 * @param v
	 */
	public BadArgumentTypeException(TeclaVirtual v) {
		super(v);
	}

	/* (non-Javadoc)
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		
		return ICalcErr.BadArgumentType;
	}

}
