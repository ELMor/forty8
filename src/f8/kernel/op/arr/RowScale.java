package f8.kernel.op.arr;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Matrix;
import f8.types.utils.DoubleValue;
import f8.types.utils.IntValue;

public final class RowScale extends NonAlgebraic {
   public RowScale() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 71;
   }

   public Storable getInstance() {
      return new RowScale();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   // m i c rowscale
   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(3)) {
         Stackable M=cl.peek(2);
         Command         I=cl.peek(1);
         Command         C=cl.peek(0);

         if(
            M instanceof Matrix&&I instanceof IntValue
                &&C instanceof DoubleValue
           ) {
            Matrix m=(Matrix)M;
            int    i=((IntValue)I).intValue();
            double c=((DoubleValue)C).doubleValue();
            cl.pop(3);

            for(int k=0; k<m.c; k++) {
               m.x[i][k]*=c;
            }

            cl.push(M);
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("ROWSCALE");
   }
}
