package f8.kernel.op.arr;


public final class MatrixOperations {
   static void reduce(double[][] matrix, int rows, int columns) {
      int r=0;
      int c=0;

      while((r<rows)&&(c<columns)) {
         // find pivot in column c below row r
         int    p  =rows;
         double max=0;
         double x;

         for(int i=r; i<rows; i++) {
            if((x=Math.abs(matrix[i][c]))>max) {
               max   =x;
               p     =i;
            }
         }

         if(p<rows) {
            // pivot 
            double[] y=matrix[p];
            double   z=matrix[p][c];
            matrix[p]      =matrix[r];
            matrix[r]      =y;
            matrix[r][c]   =1.0;

            for(int j=c+1; j<matrix[r].length; j++) {
               matrix[r][j]/=z;
            }

            for(int i=r+1; i<rows; i++) {
               x         =matrix[i][c];
               matrix[i][c]   =0.0;

               for(int j=c+1; j<matrix[i].length; j++) {
                  matrix[i][j]-=(x*matrix[r][j]);
               }
            }

            r++;
            c++;
         } else {
            c++;
         }
      }
   }

   public static double[][] matrixCopy(double[][] m) {
      double[][] n=new double[m.length][];

      for(int i=0; i<m.length; i++) {
         n[i]=new double[m[i].length];

         for(int j=0; j<m[i].length; j++) {
            n[i][j]=m[i][j];
         }
      }

      return (n);
   }

   static double dot(double[] u, double[] v, int k) {
      double sum=0.0;

      for(int i=k; i<u.length; i++) {
         sum+=(u[i]*v[i]);
      }

      return (sum);
   }

   // u = m*v (u not the same as v)
   static void product(double[] u, double[][] m, double[] v, int i) {
      int n=v.length;

      for(int j=i; j<n; j++) {
         double sum=0.0;

         for(int k=i; k<n; k++) {
            sum+=(m[j][k]*v[k]);
         }

         u[j]=sum;
      }
   }

   /** Reduces M to tridiagonal form by a sequence of reflections.
      See Parlett, p. 118.
      (1) If u = first column below diagonal,
      then reflect in w = (u + |u|*(sign u*e) e) to take u to s|u|e,
      where e = [1,0,0,...,0].
      (2) Let gamma = 2/|w|^2
              p := gamma.M.w
              q := p - w(gamma w dot p)/2
      (3) Then HMH = M - wq^{t} - qw^{t}
    */
   public static double[][] toTridiagonal(double[][] M) {
      int      n=M.length;
      double[] w=new double[n];
      double[] p=new double[n];

      for(int i=0; i<(n-1); i++) {
         for(int j=i+1; j<n; j++) {
            w[j]      =M[j][i];
            M[j][i]   =0;
            M[i][j]   =0;
         }

         double norm=dot(w, w, i+1);

         if(norm!=0) {
            double beta=Math.sqrt(norm);

            if(w[i+1]<0) {
               beta=-beta;
            }

            M[i+1][i]   =-beta;
            M[i][i+1]   =-beta;
            norm        =2*(norm+(beta*w[i+1]));
            w[i+1]+=beta;

            double gamma=2/norm;
            product(p, M, w, i+1);

            double d=dot(w, p, i+1);

            for(int j=i+1; j<n; j++) {
               p[j]=gamma*(p[j]-(0.5*gamma*d*w[j]));
            }

            for(int j=i+1; j<n; j++) {
               M[j][j]-=(2*w[j]*p[j]);
            }

            for(int j=i+2; j<n; j++) {
               for(int k=i+1; k<j; k++) {
                  M[j][k]-=((w[j]*p[k])+(w[k]*p[j]));
                  M[k][j]=M[j][k];
               }
            }
         }
      }

      return (M);
   }

}
