package f8.kernel.op.arr;

import f8.CL;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Matrix;
import f8.types.utils.DoubleValue;
import f8.types.utils.IntValue;

public final class RowSub extends NonAlgebraic {
   public RowSub() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 72;
   }

   public Storable getInstance() {
      return new RowSub();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   // m i j c rowsub
   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      if(cl.check(4)) {
         Stackable m=cl.peek(3);
         Stackable I=cl.peek(2);
         Stackable J=cl.peek(1);
         Stackable C=cl.peek(0);

         if(
            m instanceof Matrix&&I instanceof IntValue&&J instanceof IntValue
                &&C instanceof DoubleValue
           ) {
            Matrix M=(Matrix)m;
            int    i=((IntValue)I).intValue();
            int    j=((IntValue)J).intValue();
            double c=((DoubleValue)C).doubleValue();
            cl.pop(4);

            for(int k=0; k<M.c; k++) {
               M.x[i][k]-=(c*M.x[j][k]);
            }

            cl.push(m);
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public String toString() {
      return ("ROWSUB");
   }
}
