package f8.storage;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.Storable;
import f8.types.Directory;
import f8.types.InfixExp;
import f8.types.Literal;

public final class PGDIR extends NonAlgebraic {
   public PGDIR() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 509;
   }

   public Storable getInstance() {
      return new PGDIR();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(cl.check(1)) {
         Command   a         =cl.peek(0);
         String    name;
         Hashtable itemHolder=cl.currentDict().getHT();
         if(a instanceof Literal) {
            name=((Literal)a).nombre;
            removeDir(cl, itemHolder, name);
         } else if(a instanceof InfixExp&&((name=InfixExp.isLiteral(a))!=null)) {
            removeDir(cl, itemHolder, name);
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   private void removeDir(CL cl, Hashtable itemHolder, String name)
                   throws F8Exception {
      cl.pop();
      if((Stackable)itemHolder.get(name) instanceof Directory) {
         itemHolder.remove(name);
      } else {
         throw new BadArgumentTypeException(this);
      }
      StaticCalcGUI.theGUI.refreshProgMenu();
   }

   public String toString() {
      return ("PGDIR");
   }
}
