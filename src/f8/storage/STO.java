package f8.storage;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.CircularReferenceException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.op.keyboard.mkl;
import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.Storable;
import f8.platform.Vector;
import f8.types.InfixExp;
import f8.types.Literal;

public final class STO extends NonAlgebraic {
   public STO() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 5;
   }

   public Storable getInstance() {
      return new STO();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(cl.check(2)) {
         Command a   =cl.peek(0);
         Command b   =cl.peek(1);
         String  name;
         if(a instanceof Literal) {
            name=((Literal)a).nombre;
            cl.pop(2);
            storeVar(name, b);
         } else if(a instanceof InfixExp&&((name=InfixExp.isLiteral(a))!=null)) {
				cl.pop(2);
            storeVar(name, b);
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public static void storeVar(String name, Command b) throws CircularReferenceException {
      Stackable j;
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      try {
         j=(Stackable)b;
         if(j instanceof InfixExp) {
            Vector incog=mkl.incogOf(cl, j.getAST());
            if(incog.find(name)>=0) {
               throw new CircularReferenceException(new STO());
            }
         }
      } catch(ClassCastException cce) {
      }
      Hashtable h=cl.currentDict().getHT();
      h.put(name, b);
      StaticCalcGUI.theGUI.refreshProgMenu();
   }

   public String toString() {
      return ("STO");
   }
}
