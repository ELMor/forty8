package f8.storage;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.Stackable;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.NonEmptyDirException;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Hashtable;
import f8.platform.Storable;
import f8.types.Directory;
import f8.types.InfixExp;
import f8.types.Lista;
import f8.types.Literal;

public final class PURGE extends NonAlgebraic {
   public PURGE() {
      //Aqui estaba systemInstall
   }

   public int getID() {
      return 510;
   }

   public Storable getInstance() {
      return new PURGE();
   }

   public void loadState(DataStream ds) {
   }

   public void saveState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(cl.check(1)) {
         Command a   =cl.peek(0);
         String  name;
         if(a instanceof InfixExp&&((name=InfixExp.isLiteral(a))!=null)) {
            cl.pop();
            purgeVar(cl, name);
         } else if(a instanceof Lista) {
            cl.pop();
            Lista la=(Lista)a;
            for(int i=0; i<la.size(); i++) {
               if(la.get(i) instanceof Literal) {
                  name=((Literal)la.get(i)).nombre;
                  Hashtable itemHolder=cl.currentDict().getHT();
                  if((Stackable)itemHolder.get(name) instanceof Directory) {
                     throw new NonEmptyDirException(this);
                  } else {
                     itemHolder.remove(name);
                  }
               } else {
                  throw new BadArgumentTypeException(this);
               }
            }
            StaticCalcGUI.theGUI.refreshProgMenu();
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public static void purgeVar(CL cl, String name) throws NonEmptyDirException {
      Hashtable itemHolder=cl.currentDict().getHT();
      if((Stackable)itemHolder.get(name) instanceof Directory) {
         throw new NonEmptyDirException(new PURGE());
      } else {
         itemHolder.remove(name);
      }
      StaticCalcGUI.theGUI.refreshProgMenu();
   }

   public String toString() {
      return ("PURGE");
   }
}
