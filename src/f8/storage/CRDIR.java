/*
 * Created on 07-ago-2003
 *
 
 
 */
package f8.storage;

import f8.CL;
import f8.Command;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.types.Directory;
import f8.types.InfixExp;
import f8.types.Literal;

/**
 * @author elinares
 *

 
 */
public final class CRDIR extends NonAlgebraic {
   public CRDIR() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getID()
    */
   public int getID() {
      return 125;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getInstance()
    */
   public Storable getInstance() {
      return new CRDIR();
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
    */
   public void saveState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
    */
   public void loadState(DataStream ds) {
   }

   public void exec() throws F8Exception {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();

      if(cl.check(1)) {
         Command a   =cl.peek(0);
         String  name;

         // define a to be b
         if(a instanceof Literal) {
            name=a.toString();
				cl.pop(1);
            createDir(cl, name);
         } else if((name=InfixExp.isLiteral(a))!=null) {
				cl.pop(1);
            createDir(cl, name);
         } else {
            throw new BadArgumentTypeException(this);
         }
      } else {
         throw new TooFewArgumentsException(this);
      }
   }

   public static void createDir(CL cl, String name) {
      new Directory(cl.currentDict(), name);
      StaticCalcGUI.theGUI.refreshProgMenu();
   }

   public String toString() {
      return "CRDIR";
   }
}
