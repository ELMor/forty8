/*
 * Created on 07-ago-2003
 *
 
 
 */
package f8.storage;

import f8.CL;
import f8.NonAlgebraic;
import f8.StaticCalcGUI;
import f8.platform.DataStream;
import f8.platform.Storable;
import f8.ui.CF;


/**
 * @author elinares
 *

 
 */
public final class HOME extends NonAlgebraic {
   public HOME() {
      //Aqui estaba systemInstall
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getID()
    */
   public int getID() {
      return 124;
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#getInstance()
    */
   public Storable getInstance() {
      return new HOME();
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
    */
   public void saveState(DataStream ds) {
   }

   /* (non-Javadoc)
    * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
    */
   public void loadState(DataStream ds) {
   }

   public void exec() {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      cl.homeDir();
      CF cf=StaticCalcGUI.theGUI.getCalcFisica();
      StaticCalcGUI.theGUI.refreshProgMenu();
   }

   public String toString() {
      return "HOME";
   }
}
