/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui;


import f8.platform.CalcGUI;

/**
 * @author elinares
 *

 
 */
public final class NXT extends Tecla {
   public NXT() {
      super("NEXT"); //NXT
   }

   public void pressed(CalcGUI cg) {
      CF cf=cg.getCalcFisica();
      cf.nxtUpMenuPage();
      cg.switchButtonGroup(1);
   }
}
