/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui;


import f8.platform.CalcGUI;

/**
 * @author elinares
 *

 
 */
public final class PREV extends Tecla {
   public PREV() {
      super("PREV");
   }

   public void pressed(CalcGUI cg) {
      CF cf=cg.getCalcFisica();
      cf.prevUpMenuPage();
      cg.switchButtonGroup(1);
   }
}
