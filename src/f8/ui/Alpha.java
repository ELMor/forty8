/*
 * Created on 28-jul-2003
 *
 
 
 */
package f8.ui;


import f8.platform.CalcGUI;
import f8.ui.hp48.keyb.Keyboard;

/**
 * @author elinares
 *

 
 */
public final class Alpha extends Tecla {
   public Alpha() {
      super("\u008C");
   }

   public void pressed(CalcGUI cg) {
      CF cf=cg.getCalcFisica();
      cf.setAlphaMode(cf.getAlphaMode()+1);
      if(cg.getCalcFisica().getAlphaMode()>2) {
         cg.getCalcFisica().setAlphaMode(0);
      }
      if(cg.getCalcFisica().getAlphaMode()>0) {
         cg.setMenu(0, Keyboard.alphUp, Keyboard.alphMiddle, null);
      } else {
         cg.setMenu(0, null, Keyboard.normMiddle, Keyboard.normDown);
      }
   }
}
