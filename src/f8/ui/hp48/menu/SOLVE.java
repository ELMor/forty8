/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui.hp48.menu;

import antlr.collections.AST;

import f8.CL;
import f8.Operation;
import f8.Stackable;
import f8.StaticCalcGUI;

import f8.kernel.rtExceptions.BadArgumentTypeException;
import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.TooFewArgumentsException;
import f8.kernel.rtExceptions.UndefinedException;

import f8.kernel.yacc.ObjectParserTokenTypes;

import f8.op.keyboard.mkl;

import f8.platform.CalcGUI;
import f8.platform.Vector;

import f8.solver.EqSolv;
import f8.storage.STO;


import f8.types.InfixExp;
import f8.types.Literal;

import f8.types.utils.CommandSequence;

import f8.ui.CF;
import f8.ui.Menu;
import f8.ui.Tecla;
import f8.ui.TeclaVirtual;

/**
 * @author elinares
 *

 
 */
public final class SOLVE extends Menu implements ObjectParserTokenTypes {
   public SOLVE() throws F8Exception {
      super("SOLVE");

      try {
         appendKey(solverWithEQDefined());
      } catch(UndefinedException e) {
         StaticCalcGUI.theGUI.output("No EQ, store one!");
         appendKey(solverWithoutEQDefined());
      }

      appendKey(new Tecla(
                          "ROOT",
                          new Operation() {
            /* (non-Javadoc)
             * @see f8.kernel.Operation#exec()
             */
            public void exec() {
               // TODO Auto-generated method stub
            }
         }
                         )
               );
      appendKey(new Tecla(
                          "NEW",
                          new Operation() {
            /* (non-Javadoc)
             * @see f8.kernel.Operation#exec()
             */
            public void exec() {
               // TODO Auto-generated method stub
            }
         }
                         )
               );
      appendKey(new Tecla(
                          "EDEQ",
                          new Operation() {
            /* (non-Javadoc)
             * @see f8.kernel.Operation#exec()
             */
            public void exec() {
               // TODO Auto-generated method stub
            }
         }
                         )
               );
      appendKey(new Tecla("STEQ"));
      appendKey(new Tecla(
                          "CAT",
                          new Operation() {
            /* (non-Javadoc)
             * @see f8.kernel.Operation#exec()
             */
            public void exec() {
               // TODO Auto-generated method stub
            }
         }
                         )
               );
   }
	private TeclaVirtual solverWithoutEQDefined() throws F8Exception{
		return new Tecla("SOLVR", new Operation(){
		/* (non-Javadoc)
		 * @see f8.Operation#exec()
		 */
		public void exec() throws F8Exception {
			CL cl=StaticCalcGUI.theGUI.getCalcLogica();
			Stackable eq=(Stackable)cl.lookup("EQ");
			if(eq!=null) {
				StaticCalcGUI.theGUI.setMenu(1,new SOLVE(),null,null);
				StaticCalcGUI.theGUI.getCalcFisica().key(0);				
			}
		}
});
	}
	
   private Menu solverWithEQDefined() throws F8Exception {
      Menu m =new Menu("SOLVR");
      CL   cl=StaticCalcGUI.theGUI.getCalcLogica();
      try {
         Stackable eq=(Stackable)cl.lookup("EQ");
         if(eq==null) {
            throw new UndefinedException(SOLVE.this);
         }
         if(eq instanceof InfixExp) {
            InfixExp ieq=(InfixExp)eq;
            Vector   v=mkl.incogOf(cl, ieq.getAST());
            for(int i=0; i<v.getCount(); i++) {
               String vName=(String)v.elementAt(i);
               m.appendKey(new Tecla(vName, new varSolverKey(vName), true));
            }
            if(ieq.getAST().getType()==ASSIGN) {
               AST fc=ieq.getAST().getFirstChild();
               m.appendKey(new Tecla("LEFT", new expSolverKey("LEFT", fc)));
               m.appendKey(new Tecla(
                                     "RIGHT",
                                     new expSolverKey(
                                                      "RIGHT",
                                                      fc.getNextSibling()
                                                     )
                                    )
                          );
            } else {
               m.appendKey(new Tecla(
                                     "EXPR",
                                     new expSolverKey("EXPR", ieq.getAST())
                                    )
                          );
            }
            return m;
         } else {
            throw new BadArgumentTypeException(SOLVE.this);
         }
      } catch(ClassCastException k) {
         throw new BadArgumentTypeException(SOLVE.this);
      }
   }

   private class varSolverKey extends Operation {
      String nombre;

      public varSolverKey(String name) {
         nombre=name;
      }

      public void exec() throws F8Exception {
         CalcGUI cg=StaticCalcGUI.theGUI;
         CF      cf=cg.getCalcFisica();
         CL      cl=cg.getCalcLogica();
         if(!StaticCalcGUI.theGUI.getEditField().equals("")) {
            cl.enter(StaticCalcGUI.theGUI.getEditField());
         }
         switch(cf.getShiftMode()) {
            case 0: //Normal
               if(cl.check(1)) {
                  String val=cl.peek().toString();
                  cl.push(new Literal(nombre));
                  new STO().exec();
                  cg.temporaryLabels("", nombre+":"+val);
               } else {
                  throw new TooFewArgumentsException(SOLVE.this);
               }
               break;
            case 1:
               //Aqui se inicia el solver
               EqSolv.solve((InfixExp)cl.lookup("EQ"), nombre);
               break;
            case 2:
               Stackable k=(Stackable)cl.lookup(nombre);
               if(k==null) {
                  cl.push(new Literal(nombre));
               } else {
                  cl.push(k);
               }
               break;
         }
         StaticCalcGUI.theGUI.refresh(true);
      }
   }

   private class expSolverKey extends Operation {
      AST    exp;
      String nombre;

      public expSolverKey(String name, AST e) {
         exp      =e;
         nombre   =name;
      }

      public void exec() throws F8Exception {
         CL              cl =StaticCalcGUI.theGUI.getCalcLogica();
         CommandSequence run=InfixExp.decode(exp);
         run.exec();
         cl.peek().setTag(nombre);
         StaticCalcGUI.theGUI.refresh(false);
      }
   }
}
