/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.ui.hp48.menu;

import f8.ui.Menu;
import f8.ui.Tecla;

/**
 * @author elinares
 *

 
 */
public final class LIBR extends Menu {
   public LIBR() {
      super("LIBR");
      appendKey(new Tecla("Port0"));
      appendKey(new Tecla("Port1"));
   }
}
