/*
 * Created on 29-ago-2003
 *
 
 
 */
package f8.ui.hp48.keyb;

import f8.ui.Menu;

/**
 * @author elinares
 *

 
 */
public final class Keyboard {
	public static Menu alphUp     =new AlphUp();

	public static Menu normMiddle =new NormMiddle();
   public static Menu alphMiddle =new AlphMiddle();
   public static Menu leftMiddle =new LeftMiddle();
   public static Menu rightMiddle=new RightMiddle();

   public static Menu normDown   =new NormDown();
   public static Menu leftDown   =new LeftDown();
   public static Menu rightDown  =new RightDown();
   
	/*
   public static String keyText(int i){
   	CF cf=StaticCalcGUI.theGUI.getCalcFisica();
   	Menu m=cf.getUpMenu();
   	boolean alpha=(1==cf.getAlphaMode());
   	int shift=cf.getShiftMode();
   	String ret=null;
   	
		if(i>=0 && i<=5){ //Programmable buttons
			if(alpha)
				ret=alphUp.getContentAt(i).getTitle();
			else
				ret=m.getContentAt(i).getTitle();
		}else if(i>=6 && i<=28){
			i-=6;
			switch(shift){
				case 0: ret=normMiddle.getContentAt(i).getTitle(); break;
				case 1: ret=leftMiddle.getContentAt(i).getTitle(); break;
				case 2: ret=rightMiddle.getContentAt(i).getTitle(); break;
			}
		} else if(i>=29 && i<=49){
			i-=29;
			switch(shift){
				case 0: ret=normDown.getContentAt(i).getTitle(); break;
				case 1: ret=leftDown.getContentAt(i).getTitle(); break;
				case 2: ret=rightDown.getContentAt(i).getTitle(); break;
			}
		}
		return ret;
   }
   */
}
