/*
 * Created on 29-sep-2003
 *
 
 
 */
package f8;

import antlr.collections.AST;
import f8.constants.PI;
import f8.kernel.rtExceptions.F8Exception;
import f8.op.keyboard.Derivable;
import f8.op.keyboard.mkl;
import f8.types.InfixExp;

/**
 * @author elinares
 *

 
 */
public abstract class Trigonometric extends Dispatch1 implements Derivable {
	public abstract AST deriveWithArgs(AST expr, String var) throws F8Exception;

   public Stackable RAD2Settings(Stackable RADAngle) {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      switch(cl.getSetting(CL.ANG_MOD)) {
         case CL.DEG:
            return new InfixExp(mkl.mul(
                                        RADAngle.getAST(),
                                        mkl.div(
                                                mkl.num(180), new PI().getAST()
                                               )
                                       )
                               );
         case CL.GRAD:
            return new InfixExp(mkl.mul(
                                        RADAngle.getAST(),
                                        mkl.div(
                                                mkl.num(200), new PI().getAST()
                                               )
                                       )
                               );
      }
      return RADAngle;
   }

   public double RAD2Settings(double RADAngle) {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      switch(cl.getSetting(CL.ANG_MOD)) {
         case CL.DEG:
            return (RADAngle*180)/Math.PI;
         case CL.GRAD:
            return (RADAngle*200)/Math.PI;
      }
      return RADAngle;
   }

   public Stackable Settings2RAD(Stackable UnkAngle) {
      CL cl=StaticCalcGUI.theGUI.getCalcLogica();
      switch(cl.getSetting(CL.ANG_MOD)) {
         case CL.DEG:
            return new InfixExp(mkl.mul(
                                        UnkAngle.getAST(),
                                        mkl.div(
                                                new PI().getAST(), mkl.num(180)
                                               )
                                       )
                               );
         case CL.GRAD:
            return new InfixExp(mkl.mul(
                                        UnkAngle.getAST(),
                                        mkl.div(
                                                new PI().getAST(), mkl.num(200)
                                               )
                                       )
                               );
      }
      return UnkAngle;
   }
   public double Settings2RAD(double UnkAngle) {
	  CL cl=StaticCalcGUI.theGUI.getCalcLogica();
	  switch(cl.getSetting(CL.ANG_MOD)) {
		 case CL.DEG:
			return Math.PI/180*UnkAngle;
		 case CL.GRAD:
			return Math.PI/200*UnkAngle;
	  }
	  return UnkAngle;
   }
	/* (non-Javadoc)
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/* (non-Javadoc)
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
