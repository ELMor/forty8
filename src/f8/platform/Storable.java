package f8.platform;

public interface Storable {
   public int getID();

   public Storable getInstance();

   public void saveState(DataStream ds);

   public void loadState(DataStream ds);
}
