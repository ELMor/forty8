/*
 * Created on 01-ago-2003
 *
 
 
 */
package f8.platform;


/**
 * @author elinares
 *

 
 */
public interface Hashtable {
   public abstract void clear();

   public abstract Object get(Object key);

   public abstract Vector getKeys();

   public abstract Object put(Object key, Object value);

   public abstract Object remove(Object key);

   public abstract int size();
}
