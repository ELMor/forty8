/*
 * Created on 31-jul-2003
 *
 
 
 */
package f8.platform;

import f8.CL;
import f8.ui.CF;
import f8.ui.Menu;

/**
 * @author elinares
 *

 
 */
public interface CalcGUI {

   /**
    * Coloca str al final de la cadena de edicion
    * @param str Cadena a a�adir
    */
   public abstract void emit(String str, int backSpaces);
   
   public boolean flush();

   /**
    * Obtiene la calculadora fisica (@see f8.fisica.CalcFisica)
    * @return la calculadora fisica.
    */
   public abstract CF getCalcFisica();

   /**
    * Obtiene la calculadora Logica
    * @return calculadora Logica (@see f8.logica.CalcLogica)
    */
   public abstract CL getCalcLogica();

   /**
    * contenido del campo de edicion
    * @return El contenido del campo de edicion
    */
   public abstract String getEditField();
   
   /**
    * Obtener el control LCD para dibujar graficos y plots
    * @return
    */
   public LCDControl getLCD();

   /**
    * Realiza un loggin
    * @param txt Texto a logear
    */
   public abstract void log(String txt);

   /**
    * Muestra un popup
    * @param errNumber Codigo de ICalcErr cuyo texto muestra
    */
   public abstract void output(int errNumber);

   /**
    * Muestra un popup
    * @param msg Mensaje del popup
    */
   public abstract void output(String msg);

   /**
    * Debe refrescar el GUI de la calculadora
    * @param clearEditField Si true, borrar edit
    */
   public abstract void refresh(boolean clearEditField);

   /**
    * Fuerza un refresco de los textos y dinujos de las 6 primeras teclas
    * (las programables)
    *
    */
   public void refreshProgMenu();
   
   
   public void refreshStatusLabels();

   /**
    * Coloca la calculadora logica
    * @param cl
    */
   public void setCalcLogica(CL cl);
   
   public void setInitialEdit(String s);

   /**
    * Coloca los textos en las teclas de la siguiente manera.
    * Si menuNumber=0, entonces coloca tanto el menu programable (6) primeras teclas
    * como la siguiente secuencia de teclas (de la 6 a la 29) y la ultima (30 a 49)
    * respectivamente m1,m2 y m3
    * Si menuNumber=1 solo coloca el menu programable (6 primeras teclas) con m1
    * Si menuNumber=2 solo coloca las teclas de enmedio (6 a 29) con m1
    * Si menuNumber=3 solo coloca las teclas de abajo (30 a 49)
    * @param menuNumber
    * @param m1
    * @param m2
    * @param m3
    */
   public abstract void setMenu(int menuNumber, Menu m1, Menu m2, Menu m3);

   /**
    * Recalcula las teclas segun groupNumber (@see setMenu).
    * @param groupNumber
    */
   public abstract void switchButtonGroup(int groupNumber);
   
   public void temporaryLabels(String label1, String label2);
   
   public abstract void setLastMenu(); 
   
   public abstract void selectUpperTab(int n);
}
