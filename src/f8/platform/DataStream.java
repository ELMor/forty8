/*
 * Created on 02-ago-2003
 *
 
 
 */
package f8.platform;


/**
 * @author elinares
 *

 
 */
public interface DataStream {
   public byte readByte();

   public boolean readBoolean();

   public int readInt();

   public String readStringSafe();

   public double readDouble();

   public int writeByte(byte b);

   public int writeBoolean(boolean b);

   public int writeInt(int i);

   public int writeStringSafe(String s);

   public int writeDouble(double d);

   public boolean close();
   
   public boolean setRecordPos(int recNumber);
   
   public boolean deleteRecord();
   
   public boolean addRecord();
}
