/*
 * Created on 01-ago-2003
 *
 
 
 */
package f8.platform;


/**
 * @author elinares
 *

 
 */
public interface Stack {
   public abstract Object push(Object k);

   public abstract int size();

   public abstract Object pop();

   public abstract Object peek();

   public abstract void poke(int i, Object k);

   public abstract Object elementAt(int i);

   public abstract void pop(int i);
}
