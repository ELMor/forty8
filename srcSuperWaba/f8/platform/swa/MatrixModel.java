/*
 * Created on 18-sep-2003
 *
 
 
 */
package f8.platform.swa;

import f8.platform.UtilFactory;
import f8.platform.Vector;
import superwaba.ext.xplat.ui.GridModel;

/**
 * @author elinares
 *

 
 */
public class MatrixModel implements GridModel {
	public Vector rows=UtilFactory.newVector();
	/**
	 * 
	 */
	public MatrixModel() {
		super();
		
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getColumnCount()
	 */
	public int getColumnCount() {
		if(rows.size()<5)
			return 5;
		Vector row=(Vector)rows.elementAt(0);
		return row.size();
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getItemCount()
	 */
	public int getItemCount() {
		if(rows.size()<5){
			return 5;
		}
		Vector row=(Vector)rows.elementAt(0);
		return rows.size()*row.size();
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getPreferredColumnWidth(int)
	 */
	public int getPreferredColumnWidth(int columnIndex) {
		return 40;
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getPreferredRowHeight(int)
	 */
	public int getPreferredRowHeight(int rowIndex) {
		return 15;
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getRowCount()
	 */
	public int getRowCount() {
		if(rows.size()<5)
			return 5;
		return rows.size();
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getSelected(int, int)
	 */
	public Object getSelected(int rowIndex, int colIndex) {
		if(rowIndex>=rows.size())
			return "";
		Vector row=(Vector)rows.elementAt(rowIndex);
		if(colIndex>=row.size())
			return "";
		return row.elementAt(rowIndex);
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getValidChars(int, int)
	 */
	public String getValidChars(int rowIndex, int colIndex) {
		return "0123456789.";
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		return getSelected(rowIndex,columnIndex);
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#isHeader(int)
	 */
	public boolean isHeader(int rowIndex) {
		return false;
	}

	/* (non-Javadoc)
	 * @see superwaba.ext.xplat.ui.GridModel#setValueAt(java.lang.Object, int, int)
	 */
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		while(rowIndex<=rows.size())
			rows.add(UtilFactory.newVector());
		Vector row=(Vector)rows.elementAt(rowIndex);
		while(columnIndex<=row.size())
			for(int i=0;i<rows.size();i++)
				((Vector)rows.elementAt(i)).add("");
		row.set(columnIndex,aValue);
	}

}
