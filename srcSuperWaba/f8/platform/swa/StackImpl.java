/*
 * Created on 12-jun-2003
 *
 
 
 */
package f8.platform.swa;

import f8.platform.Stack;


/**
 * @author elinares
 *

 
 */
public final class StackImpl implements Stack
{
	waba.util.Vector data=new waba.util.Vector();

   public Object push(Object k)
   {
   	data.push(k);
   	return k;
   }

   public int size()
   {
      return data.size();
   }

   public Object pop()
   {
   	return data.pop();
   }

   public Object peek()
   {
   	return data.peek();
   }

   public void poke(int i, Object k)
   {
   	data.setElementAt(k,data.size()-1-i);
   }

   public Object elementAt(int i)
   {
   	return data.elementAt(i);
   }

   public void pop(int i)
   {
      for(int j=0;j<i;j++)
      	data.pop();
   }
}
