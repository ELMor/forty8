/*
 * Created on 02-ago-2003
 *
 
 
 */
package f8.platform.swa;

import waba.sys.Convert;
import f8.platform.MathKernel;

/**
 * @author elinares
 *

 
 */
public final class MathKernelImpl implements MathKernel{
	public boolean isNegativeInfinite(double d){
		return Convert.doubleToLongBits(d)==0xfff0000000000000L;
	}
	public boolean isNaN(double d){
		return Convert.doubleToLongBits(d)==0x7ff8000000000000L;
	}
	public double toDouble(String s){
		return Convert.toDouble(s);
	}
	public int toInt(String s){
		return Convert.toInt(s);
	}
	public float toFloat(String s){
		return Convert.toFloat(s);
	}
	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#fromBIN(java.lang.String)
	 */
	public int fromBIN(String s) {
		return (int)Convert.toLong(s,2);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#fromDEC(java.lang.String)
	 */
	public int fromDEC(String s) {
		return (int)Convert.toLong(s,10);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#fromHEX(java.lang.String)
	 */
	public int fromHEX(String s) {
		return (int)Convert.toLong(s,16);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#fromOCT(java.lang.String)
	 */
	public int fromOCT(String s) {
		return (int)Convert.toLong(s,8);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toBIN(int)
	 */
	public String toBIN(int d) {
		return Convert.toString(d,2);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toDEC(int)
	 */
	public String toDEC(int d) {
		return Convert.toString(d,10);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toENG(double, int)
	 */
	public String toENG(double d, int digits) {
		return Convert.toString(d,digits);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toFIX(double, int)
	 */
	public String toFIX(double d, int decDigits) {
		return Convert.toString(d,decDigits);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toHEX(int)
	 */
	public String toHEX(int d) {
		return Convert.toString(d,16);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toOCT(int)
	 */
	public String toOCT(int d) {
		return Convert.toString(d,8);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toSCI(double, int)
	 */
	public String toSCI(double d, int digits) {
		return Convert.toString(d,digits);
	}

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#toSTD(double)
	 */
	public String toSTD(double d) {
		String ret=""+d;
		if(ret.endsWith(".0"))
			ret=ret.substring(0,ret.length()-2);
		return ret;
	}
	
	

	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#format(double, int, int)
	 */
	public String format(double d, int mode, int digits) {
		switch(mode){
			case 1: return toSTD(d);
			case 2: return toFIX(d,digits);
			case 3: return toSCI(d,digits);
			case 4: return toENG(d,digits);
		}
		return toSTD(d);
	}
	/* (non-Javadoc)
	 * @see f8.platform.mth.MathKernel#format(int, int)
	 */
	public String format(int d, int mode) {
		switch(mode) {
		   case 1:
			  return toDEC(d);
		   case 2:
			  return toHEX(d);
		   case 3:
			  return toOCT(d);
		   case 4:
			  return toBIN(d);
		}
		return toDEC(d);
	}

}
