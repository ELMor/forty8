/*
 * Created on 02-ago-2003
 *
 
 
 */
package f8.platform.swa;

import superwaba.ext.xplat.io.DataStream;

import waba.io.Catalog;
import waba.io.ResizeStream;

/**
 * @author elinares
 *

 
 */
public final class DataStreamImpl extends DataStream
   implements f8.platform.DataStream {
   private Catalog recordPositioning=null;	
   private ResizeStream recordStarter=null;
   	
   public DataStreamImpl(Catalog cat, ResizeStream rs) {
      super(rs);
      recordPositioning=cat;
      recordStarter=rs;
   }

   /* (non-Javadoc)
    * @see f8.platform.DataStream#readStringSafe()
    */
   public String readStringSafe() {
   	String ret=new String(readChars());
      return ret;
   }

   /* (non-Javadoc)
    * @see f8.platform.DataStream#writeStringSafe(java.lang.String)
    */
   public int writeStringSafe(String s) {
      if(s!=null) {
         return writeChars(s.toCharArray(), 0, s.length());
      } else {
         return writeShort(0);
      }
   }
	/* (non-Javadoc)
	 * @see f8.platform.DataStream#setRecordPos(int)
	 */
	public boolean setRecordPos(int recNumber) {
		return recordPositioning.setRecordPos(recNumber);
	}
	
	public boolean deleteRecord(){
		return recordPositioning.deleteRecord();
	}
	
	public boolean addRecord(){
		return recordStarter.startRecord();
	}

}
