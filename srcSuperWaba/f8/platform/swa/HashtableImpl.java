/*
 * Created on 01-ago-2003
 *
 
 
 */
package f8.platform.swa;

import f8.platform.Hashtable;
import f8.platform.Vector;


/**
 * @author elinares
 *

 
 */
public final class HashtableImpl implements Hashtable
{
   private waba.util.Hashtable h = new waba.util.Hashtable(10);

   /* (non-Javadoc)
    * @see f8.platform.util.Hashtable#clear()
    */
   public void clear()
   {
      h.clear();
   }

   /* (non-Javadoc)
    * @see f8.platform.util.Hashtable#get(java.lang.Object)
    */
   public Object get(Object key)
   {
      return h.get(key);
   }

   /* (non-Javadoc)
    * @see f8.platform.util.Hashtable#getKeys()
    */
   public Vector getKeys()
   {
   	waba.util.Vector vr=h.getKeys();
   	Vector vb=new VectorImpl();
   	int sz=vr.size();
   	for(int i=0;i<sz;i++)
   		vb.add(vr.elementAt(i));
      return vb;
   }

   /* (non-Javadoc)
    * @see f8.platform.util.Hashtable#put(java.lang.Object, java.lang.Object)
    */
   public Object put(Object key, Object value)
   {
      return h.put(key, value);
   }

   /* (non-Javadoc)
    * @see f8.platform.util.Hashtable#remove(java.lang.Object)
    */
   public Object remove(Object key)
   {
      return h.remove(key);
   }

   /* (non-Javadoc)
    * @see f8.platform.util.Hashtable#size()
    */
   public int size()
   {
      return h.size();
   }
}
