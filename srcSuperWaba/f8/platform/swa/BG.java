/*
 * Created on 05-oct-2003
 *
 
 
 */
package f8.platform.swa;

import waba.ui.PushButtonGroup;

/**
 * @author elinares
 *

 
 */
public class BG extends PushButtonGroup {

	/** create the buttons.
		@param names captions of the buttons. You can specify some names as null so the button is not displayed. This is good if you're creating a button matrix and want to hide some buttons.
		@param atLeastOne if true, at least one button must be selected
		@param selected default index to appear selected, or -1 if none
		@param gap space between the buttons, -1 glue them.
		@param insideGap space between the text and the button border. the ideal is 6.
		@param rows if > 1, creates a button matrix
		@param allSameWidth if true, all the buttons will have the width of the most large one.
		@param type can be NORMAL, BUTTON or CHECK
	*/
	public BG(
		String[] names,
		boolean atLeastOne,
		int selected,
		int gap,
		int insideGap,
		int rows,
		boolean allSameWidth,
		byte type) {
		super(
			names,
			atLeastOne,
			selected,
			gap,
			insideGap,
			rows,
			allSameWidth,
			type);
	}
	
	public void setNames(String[] nam){
		this.names=nam;
		this.repaintNow();
	}

}
