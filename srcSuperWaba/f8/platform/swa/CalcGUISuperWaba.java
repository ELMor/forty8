/*
 * Created on 12-jun-2003
 *
 
 
 */
package f8.platform.swa;

import f8.CL;
import f8.CLI2;
import f8.Command;
import f8.StaticCalcGUI;

import f8.kernel.rtExceptions.F8Exception;
import f8.kernel.rtExceptions.ICalcErr;

import f8.platform.CalcGUI;
import f8.platform.LCDControl;
import f8.platform.UtilFactory;

import f8.ui.CF;
import f8.ui.ENTER;
import f8.ui.Menu;
import f8.ui.NXT;
import f8.ui.VAR;
import f8.ui.WithEnter;

import f8.ui.hp48.HP48;
import f8.ui.hp48.keyb.Keyboard;
import f8.ui.hp48.menu.MTH;

import superwaba.ext.xplat.ui.MultiEdit;

import waba.fx.Color;
import waba.fx.Font;
import waba.fx.FontMetrics;
import waba.fx.Rect;

import waba.io.Catalog;
import waba.io.ResizeStream;

import waba.sys.Settings;
import waba.sys.Time;
import waba.sys.Vm;

import waba.ui.Button;
import waba.ui.Container;
import waba.ui.ControlEvent;
import waba.ui.Edit;
import waba.ui.Event;
import waba.ui.KeyEvent;
import waba.ui.Label;
import waba.ui.ListBox;
import waba.ui.MainWindow;
import waba.ui.MessageBox;
import waba.ui.TabPanel;
import waba.ui.Timer;

/**
 * @author elinares
 *

 
 */
public final class CalcGUISuperWaba extends MainWindow implements ICalcErr,
                                                                  CalcGUI {
   private final static String blanks=
      "                                        "
      +"                                        "
      +"                                        "
      +"                                        "
      +"                                        "
      +"                                        ";
   CF                          cf;
   CL                          cl;
   BG[]                        gxBGArray        =new BG[4];
   int[]                       gxBGArraySizes   =new int[]{18, 1, 4, 20};
   BG                          gxBGShortCut;
   Color                       gxBkColor;
   Button[]                    gxBuArrayProg    =new Button[6];
   Container                   gxCoKeysDown;
   Container                   gxCoKeysUp;
   Container                   gxCoLCD;
   Container                   gxCoMLEdit;
   Container                   gxCoStack;
   Edit                        gxEdit;
   Font                        gxFontButton;
   Font                        gxFontLabels;
   Font                        gxFontMedium;
   Font                        gxFontTiny;
   String                      gxInitEdit       ="";
   Label[]                     gxLaDirs         =new Label[6];
   Label[]                     gxLaStatus       =new Label[2];
   int                         gxLastKeyTabPanel=0;
   ListBox                     gxLBStack;
   LCDControlImpl              gxLCD            =null;
   MultiEdit                   gxMLEdit;
   int                         gxMLTabPanelIndex=2;
   int                         gxNumStackLevels =4;
   int[]        gxRectLevel=
                                                 {
                                                    0, 16, 32, 48, 64, 80, 92,
                                                    106, 120, 120, 140, 145, 146,
                                                    160, 170, 182
                                                 };
   Rect                        gxReStack;
   TabPanel                    gxTPKeys;
   TabPanel                    gxTPStackGraphics;
   boolean                     isPalm;
   boolean                     isPreStorage;
   boolean                     isTempStatusLabel=false;
   Menu lastMenu    =null;
   int  lastMenuPage;
   int                         resHNdx;
   int                         resWNdx;
   int                         screenHeight;
   int                         screenWidth;
   int								 minusBlanks=0;
   Catalog                     stCaStorage;
   DataStreamImpl              stDS;
   private final String        STORAGE_NAME     =
                                                 "f8UserStack."
                                                 +Settings.appCreatorId+".UDOS";
   ResizeStream stRS;
   Timer        theTimer;

   public CalcGUISuperWaba() {
      super(null, NO_BORDER);
      StaticCalcGUI.theGUI=this;

      //Para que el WARP las incluya
      UtilFactory.initUF(
                         new VectorImpl().getClass(),
                         new HashtableImpl().getClass(),
                         new StackImpl().getClass(),
                         new MathKernelImpl().getClass()
                        );

      //Siempre primero hay que instanciar la logica PRIMERO
      stCaStorage=new Catalog(STORAGE_NAME, Catalog.READ_ONLY);
      if(stCaStorage.isOpen()) {
         stRS   =new ResizeStream(stCaStorage, 512);
         stDS   =new DataStreamImpl(stCaStorage,stRS);
         stCaStorage.setRecordPos(0);
         isPreStorage=true;
      } else {
         isPreStorage=false;
      }
      cl         =new CLI2(isPreStorage, stDS);
      cf         =new HP48();
      theTimer   =addTimer(30000);

      //Quitar esto!!
      Command x=new RES();
      cl.systemInstall(x);
      Command.storeID(x);
      //QUITAR
   }

   private void add01_StatusStackGraphics() {
      //Label que normalmente mantiene el directorio actual
      add(gxLaStatus[1]=new Label("loading...", Label.LEFT));
      gxLaStatus[1].setFocusLess(true);
      gxLaStatus[1].setFont(gxFontLabels);
      gxLaStatus[1].setRect(0, 0, FILL, PREFERRED-1);
      //TabPanel que contiene la pila y el control LCD
      String[] sTabs={"St", "Gr"};
      gxTPStackGraphics=new TabPanel(sTabs);
      add(gxTPStackGraphics);
      gxTPStackGraphics.setFont(gxFontLabels);
      gxTPStackGraphics.setType(TabPanel.TABS_TOP);
      gxTPStackGraphics.setBackColor(gxBkColor);
      //Calculamos el RECT
      int height=gxRectLevel[gxNumStackLevels];
      gxTPStackGraphics.setRect(0, AFTER, FILL, height);
      gxTPStackGraphics.setFocusLess(true);
      //A�adimos la segunda label en la misma altura
      add(gxLaStatus[0]=new Label("loading...", Label.LEFT));
      gxLaStatus[0].setFont(gxFontLabels);
      gxLaStatus[0].setFocusLess(true);
      gxLaStatus[0].setRect(getRect().width/5, SAME+2, FILL, PREFERRED);
      //Ahora vamos con la pila
      gxCoStack=gxTPStackGraphics.getPanel(0);
      gxCoStack.setBackColor(gxBkColor);

      gxLBStack=new ListBox();
      gxCoStack.add(gxLBStack);
      gxLBStack.setRect(0, 0, FILL, FILL);
      gxLBStack.setFont(gxFontMedium);
      gxLBStack.setFocusLess(true);
      gxReStack   =gxLBStack.getRect();
      //Ahora con el control LCD
      gxCoLCD=gxTPStackGraphics.getPanel(1);
      gxCoLCD.setBackColor(gxBkColor);
      gxCoLCD.setFocusLess(true);
      Rect r=gxCoLCD.getRect();
      gxLCD=new LCDControlImpl(r.height, r.width, gxFontTiny);
      gxCoLCD.add(gxLCD);
      gxLCD.setRect(0, 0, FILL, FILL);
      gxLCD.setFocusLess(true);
   }

   private void add02_ProgrammableButtons() {
      int    butSize=(getClientRect().width-8)/6;
      int    butH=0;
      Rect   req;
      String bsz;
      switch(resWNdx) {
         case 0: //160
            bsz="WWW";
            break;
         case 1: //240
            bsz="WWW1";
            break;
         case 2: //320
         default:
            bsz="WWWWWWW";
            break;
      }
      switch(resHNdx) {
         case 0:
            butH=1;
            break;
         case 1:
            butH=3;
            break;
         case 2:default:
            butH=3;
            break;
      }
      add(gxBuArrayProg[0]=new Button(bsz));
      Rect tpr      =gxTPStackGraphics.getRect();
      int  yPosition=tpr.y+tpr.height;
      gxBuArrayProg[0].setRect(2, yPosition+1, butSize, PREFERRED-butH);
      gxBuArrayProg[0].setFocusLess(true);
      gxBuArrayProg[0].setFont(gxFontButton);
      req=gxBuArrayProg[0].getRect();
      for(int i=1; i<6; i++) {
         add(gxBuArrayProg[i]=new Button(bsz));
         gxBuArrayProg[i].setRect(AFTER+1, SAME, butSize, PREFERRED-butH);
         gxBuArrayProg[i].setFocusLess(true);
         gxBuArrayProg[i].setFont(gxFontButton);
      }
      int xx=req.x;
      int yy=req.y;
      int f =gxBuArrayProg[1].getRect().x-gxBuArrayProg[0].getRect().x;

      gxLaDirs[0]=new Label("H");
      add(gxLaDirs[0]);
      gxLaDirs[0].setRect(xx+1, yy-1, 4, 1);
      gxLaDirs[0].setFocusLess(true);
      gxLaDirs[0].setVisible(false);
      gxLaDirs[0].setBackColor(gxBkColor);
      for(int i=1; i<6; i++) {
         gxLaDirs[i]=new Label("H");
         add(gxLaDirs[i]);
         gxLaDirs[i].setRect(xx+1+(i*f), yy-1, 4, 1);
         gxLaDirs[i].setFocusLess(true);
         gxLaDirs[i].setVisible(false);
         gxLaDirs[i].setBackColor(gxBkColor);
      }
   }

   private void add03_EditControl(Rect r2) {
      add(gxEdit=new Edit());
      gxEdit.setFont(gxFontMedium);
      gxEdit.setRect(1, r2.y+r2.height+1, FILL-1, PREFERRED);
      gxEdit.setText(gxInitEdit);
   }

   private void add04_KeyTabPanel() {
      String[] sTabs;
      switch(resWNdx) {
         case 0: //160
            sTabs=new String[3];
            sTabs[0]="Do";
            sTabs[1]="Up";
            sTabs[2]="ML";
            break;
			case 1: //240
         case 2: //320
         default:
            sTabs=new String[2];
            sTabs[0]="Keyboard";
            sTabs[1]="Multiline";
            break;
      }
      gxTPKeys=new TabPanel(sTabs);
      gxTPKeys.setFont(gxFontMedium);
      add(gxTPKeys);
      gxTPKeys.setBackColor(gxBkColor);
      FontMetrics fm        =getFontMetrics(gxFontButton);
      int         monoHeight=fm.getHeight()+(2*fm.getLeading());
      int         yOff;
      switch(resHNdx) {
         case 0:
            yOff=0;
            break;
         case 1:
            yOff=3;
            break;
         case 2:
            yOff=4;
            break;
         case 3:default:
            yOff=4;
            break;
      }
      if(!isPalm) {
         gxTPKeys.setRect(0, AFTER+yOff, FILL, 7*monoHeight);
      } else {
         gxTPKeys.setRect(0, AFTER+yOff, FILL, FILL);
      }
      gxTPKeys.setFocusLess(true);

      //Ahora los botones interiores
      if(gxMLTabPanelIndex==2) {
         gxCoKeysDown=gxTPKeys.getPanel(0);
         gxCoKeysDown.setBackColor(gxBkColor);
         gxCoKeysDown.setFocusLess(true);

         gxCoKeysUp=gxTPKeys.getPanel(1);
         gxCoKeysUp.setBackColor(gxBkColor);
         gxCoKeysUp.setFocusLess(true);

         gxCoMLEdit=gxTPKeys.getPanel(2);
         gxCoMLEdit.setBackColor(gxBkColor);
         gxCoMLEdit.setFocusLess(true);

         gxCoMLEdit.add(gxMLEdit=new MultiEdit(5, 0));
         gxMLEdit.setFont(gxFontMedium);
         gxMLEdit.setRect(0, 0, FILL, FILL);
      } else {
         gxCoKeysDown=gxTPKeys.getPanel(0);
         gxCoKeysDown.setBackColor(gxBkColor);
         gxCoKeysDown.setFocusLess(true);

         gxCoKeysUp   =gxCoKeysDown;

         gxCoMLEdit=gxTPKeys.getPanel(1);
         gxCoMLEdit.setBackColor(gxBkColor);
         gxCoMLEdit.setFocusLess(true);

         gxCoMLEdit.add(gxMLEdit=new MultiEdit(5, 0));
         gxMLEdit.setFont(gxFontMedium);
         gxMLEdit.setRect(0, 0, FILL, FILL);
      }
   }

   private void add05_Buttons() {
      add0501_ShortCutButtons();
      String upBut;
      String doBut;
      switch(resWNdx) {
         case 0: //160
            upBut="AAAA";
            doBut="AAAAA";
            break;
         case 1: //240
            upBut="aaa11A";
            doBut="aaaaa1A";
            break;
         case 2: //320
         default:
            upBut="W111";
            doBut="W11aa";
            break;
      }
      add0502_createButtons(upBut, doBut);
   }

   private void add0501_ShortCutButtons() {
      if(gxMLTabPanelIndex==1) {
         return; //No hay shortcuts!
      }
      String[] names={"Left", "Right", "NXT", "ENTER"};
      gxBGShortCut=new BG(names, false, -1, 0, 6, 1, false, (byte)1);
      add(gxBGShortCut);
      double xFactor;
      int    yOff;
      int    heOff;
      int    wiOff;
      switch((resHNdx*10)+resWNdx) {
         case 0:
            xFactor=3.25;
            yOff=0;
            heOff=1;
            wiOff=0;
            break;
         case 1:
            xFactor=3.25;
            yOff=0;
            heOff=1;
            wiOff=0;
            break;
         case 2:
            xFactor=3.25;
            yOff=0;
            heOff=1;
            wiOff=0;
            break;
         case 10:
            xFactor=2.25;
            yOff=3;
            heOff=1;
            wiOff=0;
            break;
         case 11:
            xFactor=2.25;
            yOff=3;
            heOff=1;
            wiOff=0;
            break;
         case 12:
            xFactor=2.25;
            yOff=3;
            heOff=1;
            wiOff=0;
            break;
         case 20:
            xFactor=2.25;
            yOff=4;
            heOff=1;
            wiOff=0;
            break;
         case 21:
            xFactor=2.25;
            yOff=4;
            heOff=1;
            wiOff=0;
            break;
         case 22:
            xFactor=2.5;
            yOff=4;
            heOff=6;
            wiOff=0;
            break;
         case 30:
            xFactor=3;
            yOff=4;
            heOff=6;
            wiOff=0;
            break;
         case 31:
            xFactor=3;
            yOff=4;
            heOff=8;
            wiOff=0;
            break;
         default:
         case 32:
            xFactor=3;
            yOff=4;
            heOff=8;
            wiOff=0;
            break;
      }
      Rect r3=gxTPKeys.getRect();
      gxBGShortCut.setRect(
                           (int)(r3.width/xFactor), r3.y-yOff, PREFERRED-wiOff,
                           PREFERRED-heOff
                          );
      gxBGShortCut.setFocusLess(true);
      gxBGShortCut.setFont(gxFontButton);
   }

   private void add0502_createButtons(String iUp, String iDown) {
      int     UpHeight     =(gxCoKeysUp.getClientRect().height)/4;
      int     DownHeight   =1+((gxCoKeysDown.getClientRect().height)/4);
      boolean sameContainer=gxCoKeysUp==gxCoKeysDown;
      if(sameContainer) { //Estan en un solo container
         UpHeight/=2;
         DownHeight/=2;
      }
      int center3=0;
      int xof=0;

      switch(resHNdx) {
         case 0:
            break;
         case 1:
            UpHeight+=1;
            DownHeight+=0;
            break;
         case 2:
            UpHeight+=1;
            DownHeight+=0;
            break;
         case 3:default:
            UpHeight+=1;
            DownHeight+=0;
            break;
      }
      switch(resWNdx) {
         case 0:
            center3=0;
            break;
         case 1:
            center3=0;
            break;
         case 2:
            center3=2;
            xof=3;
            break;
         case 3:default:
            center3=2;
            xof=3;
            break;
      }

      //Tres primeras filas del teclado superior
      gxBGArray[0]=
              new BG(dummyText(18, iUp), false, -1, 0, 6, 3, true, (byte)1);
      gxCoKeysUp.add(gxBGArray[0]);
      gxBGArray[0].setRect(xof, 0, FILL, UpHeight*3);
      //Tecla ENTER
      gxBGArray[1]=
              new BG(dummyText(1, iUp+iUp), false, -1, 0, 6, 1, true, (byte)1);
      gxCoKeysUp.add(gxBGArray[1]);
      gxBGArray[1].setRect(
                           xof, AFTER, gxBGArray[0].getRect().width/3, UpHeight
                          );
      //Teclas a la derecha de la ENTER
      gxBGArray[2]=new BG(dummyText(4, iUp), false, -1, 0, 6, 1, true, (byte)1);
      gxCoKeysUp.add(gxBGArray[2]);
      gxBGArray[2].setRect(AFTER-center3, SAME, FILL, UpHeight);
      //Mitad inferior de las teclas
      gxBGArray[3]=
              new BG(dummyText(20, iDown), false, -1, 0, 6, 4, true, (byte)1);
      gxCoKeysDown.add(gxBGArray[3]);
      gxBGArray[3].setRect(xof, sameContainer ? AFTER : 0, FILL, FILL);
      //------------------------------
      for(int i=0; i<4; i++) {
         gxBGArray[i].setFont(gxFontButton);
         gxBGArray[i].setFocusLess(true);
      }
   }

   private String[] dummyText(int sz, String k) {
      String[] ret=new String[sz];
      for(int i=0; i<sz; i++) {
         ret[i]=k;
      }
      return ret;
   }

   public void emit(String str, int backSpaces) {
      String  edContent=gxEdit.getText();
      boolean stat=false;

      if((str.indexOf("<<")>=0)||
         (str.indexOf("\u00AB")>=0)||
         (str.indexOf("{")>=0)
        ) 
      {
         cl.setSettings(CL.PRG_MOD, true);
         stat=true;
      }
      if(str.indexOf("'")>=0) {
         cl.setSettings(CL.ALG_MOD, true);
         stat=true;
      }

      if(stat) {
         refreshStatusLabels();
      }
      int    posOfInsert=gxEdit.getCursorPos()[1];
      String oldContent=gxEdit.getText();
      gxEdit.setText(oldContent.substring(0, posOfInsert)+str
                     +oldContent.substring(posOfInsert)
                    );
      gxEdit.setCursorPos(-1, (posOfInsert+str.length())-backSpaces);
   }

   /* (non-Javadoc)
    * @see f8.platform.CalcGUI#flush()
    */
   public boolean flush() {
      boolean ret=false;
      try {
         if(!getEditField().equals("")) {
            cl.enter(getEditField());
         }
      } catch(F8Exception e) {
         refresh(true);
         ret=true;
      }

      return ret;
   }

   public CF getCalcFisica() {
      return cf;
   }

   public CL getCalcLogica() {
      return cl;
   }

   public String getEditField() {
      return gxEdit.getText();
   }

   /* (non-Javadoc)
    * @see f8.platform.CalcGUI#getLCD()
    */
   public LCDControl getLCD() {
      return gxLCD;
   }

   private Font loadFontNamed(String fontName, boolean large) {
      Font tinyFont;
      tinyFont=new Font(fontName, Font.PLAIN, large ? 14 : 6);
      if(!tinyFont.getName().equalsIgnoreCase(fontName)) {
         MessageBox mb=
                  new MessageBox(
                                 "Warning!",
                                 "Please install the|"+fontName
                                 +(large ? "Large" : "Small")+".pdb font file!",
                                 null
                                );
         mb.dontSaveBehind(false); // since the onStart method wasnt finished yet, we can't save behind
         popupModal(mb);
         Vm.sleep(2000);
         mb.unpop();
      }
      return tinyFont;
   }

   public void log(String txt) {
      Vm.debug(txt);
   }

   public void onEvent(Event event) {
      int     ind;
      Object  tgt    =event.target;
      boolean refStat=false;

      switch(event.type) {
         case KeyEvent.KEY_PRESS:
            onEvent_KeyPress(event);
            break;
         case ControlEvent.TIMER:
            refStat=onEvent_Timer(refStat);
            break;
         case ControlEvent.PRESSED:
            if(tgt==gxLBStack) {
               onEvent_PressedStack();
            } else if(tgt==gxTPKeys) {
               onEvent_PressedKeys();
            } else if((gxTPKeys.getActiveTab()==2)) {
               gxLastKeyTabPanel=gxMLTabPanelIndex;
               gxTPKeys.setActiveTab(0);
            } else if(tgt==gxBGShortCut) {
               onEvent_PressedShortcut();
            } else if(tgt instanceof BG) {
               onEvent_PressedBG(tgt);
            } else {
               onEvent_PressedDefault(tgt);
            }
      }
      if(refStat) {
         refreshStatusLabels();
      }
   }

   private void onEvent_KeyPress(Event event) {
      KeyEvent ke=(KeyEvent)event;
      if(gxTPKeys.getActiveTab()==2) { //Multiline
         gxEdit.setText(gxMLEdit.getText());
      }
   }

   private void onEvent_PressedBG(Object tgt) {
      int offset=6;
      for(int i=0; i<4; i++) {
         if(tgt==gxBGArray[i]) {
            cf.key(offset+gxBGArray[i].getSelected());
            break;
         }
         offset+=gxBGArraySizes[i];
      }
   }

   private void onEvent_PressedDefault(Object tgt) {
      for(int i=0; i<6; i++) {
         if(tgt==gxBuArrayProg[i]) {
            cf.key(i);
         }
      }
   }

   private void onEvent_PressedKeys() {
      int current=gxTPKeys.getActiveTab();
      if(
         (gxLastKeyTabPanel==gxMLTabPanelIndex)&&(current>-1)
             &&(current<gxMLTabPanelIndex)
        ) {
         gxEdit.setEnabled(true);
         gxMLEdit.setEnabled(false);
         gxEdit.setText(gxMLEdit.getText());
      } else if(
                (gxLastKeyTabPanel>-1)&&(gxLastKeyTabPanel<gxMLTabPanelIndex)
                    &&(current==gxMLTabPanelIndex)
               ) {
         gxEdit.setEnabled(false);
         gxMLEdit.setEnabled(true);
         gxMLEdit.setEditable(true);
         gxMLEdit.setText(gxEdit.getText());
      }
      gxLastKeyTabPanel=current;
   }

   private void onEvent_PressedShortcut() {
      switch(gxBGShortCut.getSelected()) {
         case 0: //LEFT
            cf.key(34);
            break;
         case 1: //RIGHT
            cf.key(39);
            break;
         case 2: //NXT
            cf.press(new NXT());
            break;
         case 3: //ENTER
            if(cf.getShiftMode()!=0) {
               cf.press(new WithEnter("DROP"));
            } else {
               cf.press(new ENTER());
            }
            refresh(true);
            break;
      }
   }

   private void onEvent_PressedStack() {
      String selected=(String)gxLBStack.getSelectedItem();
      if(selected!=null) {
         int dpIndex=selected.indexOf(':');
         do {
            dpIndex++;
         } while(selected.charAt(dpIndex)==' ');
         selected            =selected.substring(dpIndex);
         gxLastKeyTabPanel   =gxTPKeys.getActiveTab();
         gxTPKeys.setActiveTab(gxMLTabPanelIndex);
         gxMLEdit.setEditable(false);
         gxMLEdit.setText(selected);
         refresh(true);
      }
   }

   private boolean onEvent_Timer(boolean refStat) {
      if(cl.isSetting(CL.CLOCK)) {
         refStat=true;
      }
      return refStat;
   }

   public void onExit() {
      //Creamos los catalogos
      stCaStorage=new Catalog(STORAGE_NAME, Catalog.READ_WRITE);
      if(!stCaStorage.isOpen()) {
			stCaStorage   =new Catalog(STORAGE_NAME, Catalog.CREATE);
      }
      stRS          =new ResizeStream(stCaStorage, 512);
      stDS          =new DataStreamImpl(stCaStorage,stRS);
      cl.saveToStorage(gxEdit.getText(), stDS);
      stDS.close();
      stRS.endRecord();
      stCaStorage.close();
   }

   public void onStart() {
      //OK, ahora la parte gr�fica
      //Color de fondo
      gxBkColor   =getBackColor();
      //Cargamos las 2 fuentes
      gxFontTiny     =loadFontNamed("hp48gos1", false);
      gxFontMedium   =loadFontNamed("hp48gos2", false);
      //Donde estamos corriendo?
      screenHeight   =getRect().height;
      screenWidth    =getRect().width;
      isPalm         =Settings.platform.equals("PalmOS");
      isPalm         =true;
      if(isPalm) {
         if(screenHeight<=160) {
            resHNdx            =0;
            gxNumStackLevels   =4;
            gxFontButton       =gxFontTiny;
            gxFontLabels       =gxFontTiny;
				minusBlanks=4;
         } else if(screenHeight<=240) {
            resHNdx            =1;
            gxNumStackLevels   =4;
            gxFontButton       =gxFontMedium;
            gxFontLabels       =gxFontTiny;
				gxMLTabPanelIndex   =1;
				minusBlanks=4;
         } else if(screenHeight<=320) {
            resHNdx             =2;
            gxNumStackLevels    =10;
            gxFontButton        =gxFontMedium;
            gxFontLabels        =gxFontMedium;
            gxMLTabPanelIndex   =1;
				minusBlanks=4;
         } else {
            resHNdx             =3;
            gxNumStackLevels    =10;
            gxFontButton        =gxFontMedium;
            gxFontLabels        =gxFontMedium;
            gxMLTabPanelIndex   =1;
				minusBlanks=4;
         }
      } else {
         if(screenHeight<=320) {
            resHNdx             =2;
            gxNumStackLevels    =4;
            gxFontButton        =gxFontMedium;
            gxFontLabels        =gxFontMedium;
            gxMLTabPanelIndex   =1;
				minusBlanks=4;
         } else {
            resHNdx             =3;
            gxNumStackLevels    =5;
            gxFontButton        =gxFontMedium;
            gxFontLabels        =gxFontMedium;
            gxMLTabPanelIndex   =1;
				minusBlanks=4;
         }
      }
      if(screenWidth<=160) {
         resWNdx=0;
      } else if(screenWidth<=240) {
         resWNdx=1;
      } else {
         resWNdx=2;
      }

      //List box que mantiene la pila, Graphics y statusLabels
      add01_StatusStackGraphics();

      //Creamos los botoncitos (los 49), y a�adimos los 6 primeros
      add02_ProgrammableButtons();

      //El control de edicion
      Rect r2=gxBuArrayProg[5].getRect();
      add03_EditControl(r2);

      //Ahora el tabPanel de las teclas
      add04_KeyTabPanel();

      //El resto de Botones
      add05_Buttons();

      setMenu(0, new MTH(), Keyboard.normMiddle, Keyboard.normDown);
      //Refrescar
      refresh(false);
   }

   public void output(int errNumber) {
      output(ICalcErr.Name[errNumber]);
   }

   public void output(String msg) {
      if(msg==null) {
         msg="Undefined Error";
      }
      String[] but={"OK"};
      msg=msg.replace('\n', '|');
      MessageBox mb=new MessageBox("Forty8 message", msg, but);
      mb.setFont(gxFontTiny);
      popupModal(mb);
   }

   public void refresh(boolean clear) {
      if(clear) {
         gxEdit.setText("");
         cl.setSettings(CL.PRG_MOD, false);
         cl.setSettings(CL.ALG_MOD, false);
      }

      int stkSz=cl.size();
      gxLBStack.removeAll();
      FontMetrics metric       =getFontMetrics(gxFontMedium);
      int         displayHeight=gxReStack.height;

      //Corregir Rect
      int difLevels     = (stkSz>gxNumStackLevels) ? 0
                                                   : (
                                                       gxNumStackLevels
                                                       -stkSz
                                                     );
      int oneLevelHeight=displayHeight/gxNumStackLevels;

      gxLBStack.setRect(0, (difLevels*oneLevelHeight)-2, FILL, FILL);

      int with     =gxReStack.width;
      int blankWith=metric.getTextWidth(" ");
      for(int kk=0; kk<stkSz; kk++) {
         String data     =cl.peek(stkSz-1-kk).toString();
         String level    =""+(stkSz-kk)+":";
         int    dataWith =metric.getTextWidth(data);
         int    levelWith=metric.getTextWidth(level);
         int    numBlanks=((with-levelWith-dataWith)/blankWith)-minusBlanks;
         if(numBlanks>0) {
            level=level+blanks.substring(0, numBlanks);
         }
         gxLBStack.add(level+data);
      }
      if((stkSz>0)&&(gxLBStack.getSelectedIndex()==-1)) {
         gxLBStack.select(stkSz-1);
      }

      refreshStatusLabels();
   }

   /* (non-Javadoc)
    * @see f8.platform.CalcGUI#refreshProgMenu()
    */
   public void refreshProgMenu() {
      if(cf.getUpMenu() instanceof VAR) {
         setMenu(1, new VAR(), null, null);
      } else {
         switchButtonGroup(1);
      }
   }

   public void refreshStatusLabels() {
      if(isTempStatusLabel) {
         isTempStatusLabel=false;
         return;
      }
      String lab="";
      if(cf.getAlphaMode()>0) {
         lab+=" ALPH";
      }
      if(cf.getShiftMode()==1) {
         lab+=" LFT";
      }
      if(cf.getShiftMode()==2) {
         lab+=" RGT";
      }
      switch(cl.getSetting(CL.ANG_MOD)) {
         case CL.RAD:
            lab+=" RAD";
            break;
         case CL.DEG:
            lab+=" DEG";
            break;
         case CL.GRAD:
            lab+=" GRAD";
            break;
      }
      switch(cl.getSetting(CL.MOD_3D)) {
         case CL.XYZ:
            lab+=" XYZ";
            break;
         case CL.RAZ:
            lab+=" RAZ";
            break;
         case CL.RAA:
            lab+=" RAA";
            break;
      }
      if(cl.isSetting(CL.ALG_MOD)) {
         lab+=" ALG";
      }
      if(cl.isSetting(CL.PRG_MOD)) {
         lab+=" PRG";
      }
      if(cl.isSetting(CL.CLOCK)) {
         Time t=new Time();
         lab+=(" "+t.year+"."+t.month+"."+t.day+" "+t.hour+":"+t.minute);
      }
      gxLaStatus[0].setText(lab);
      gxLaStatus[1].setText(cl.getCurrentDirName());
   }

   /* (non-Javadoc)
    * @see f8.platform.CalcGUI#selectUpperTab(int)
    */
   public void selectUpperTab(int n) {
      if(n==0) {
         gxTPStackGraphics.setActiveTab(0);
      } else {
         gxTPStackGraphics.setActiveTab(1);
      }
      this.repaintNow();
   }

   public void setCalcLogica(CL cl1) {
      cl=cl1;
   }

   /* (non-Javadoc)
    * @see f8.platform.CalcGUI#setInitialEdit(java.lang.String)
    */
   public void setInitialEdit(String s) {
      gxInitEdit=s;
   }

   /* (non-Javadoc)
    * @see f8.platform.CalcGUI#setLastMenu()
    */
   public void setLastMenu() {
      if(lastMenu!=null) {
         setMenu(1, lastMenu, null, null);
         cf.setUpMenuPage(lastMenuPage);
      }
   }

   public void setMenu(int menuNumber, Menu m1, Menu m2, Menu m3) {
      if(m1!=null) {
         lastMenuPage   =cf.getUpMenuPage();
         lastMenu       =cf.getUpMenu();
      }
      switch(menuNumber) {
         case 0:
            if(m1!=null) {
               cf.setUpMenu(m1);
               switchButtonGroup(1);
            }
            if(m2!=null) {
               cf.setMiMenu(m2);
               switchButtonGroup(2);
            }
            if(m3!=null) {
               cf.setDoMenu(m3);
               switchButtonGroup(3);
            }
            break;
         case 1:
            if(m1!=null) {
               m1.notifyChangeStatus(this);
               cf.setUpMenu(m1);
               switchButtonGroup(1);
            }
            break;
         case 2:
            if(m1!=null) {
               m1.notifyChangeStatus(this);
               cf.setMiMenu(m1);
               switchButtonGroup(2);
            }
            break;
         case 3:
            if(m1!=null) {
               m1.notifyChangeStatus(this);
               cf.setDoMenu(m1);
               switchButtonGroup(3);
            }
            break;
      }
   }

   public void switchButtonGroup(int groupNumber) {
      String[] keys;
      switch(groupNumber) {
         case 1:
            //Ahora un grupo de botones menu superior
            keys=cf.getUpStringArray();
            boolean[] boo=cf.getUpIsOn();
            for(int i=0; i<6; i++) {
               gxBuArrayProg[i].setText(keys[i]+(boo[i] ? "�" : ""));
            }
            boo=cf.getUpIsDirectory();
            for(int i=0; i<6; i++) {
               gxLaDirs[i].setVisible(boo[i]);
            }
            boolean[] col=cf.getUpVarSolver();
            for(int i=0; i<6; i++) {
               if(col[i]) {
                  gxBuArrayProg[i].setBackColor(Color.WHITE);
               } else {
                  gxBuArrayProg[i].setBackColor(gxBkColor);
               }
            }
            break;
         case 2:
            //Mitad superior de botones
            Menu mm=cf.getMiMenu();
            gxBGArray[0].setNames(mm.getStringArray(0, 18));
            gxBGArray[1].setNames(mm.getStringArray(18, 1));
            gxBGArray[2].setNames(mm.getStringArray(19, 4));
            break;
         case 3:
            //Mitad Inferior de botones
            Menu md=cf.getDoMenu();
            gxBGArray[3].setNames(md.getStringArray(0, 20));
            break;
         default:
            break;
      }
   }

   public void temporaryLabels(String label1, String label2) {
      isTempStatusLabel=true;
      if(label1!=null) {
         gxLaStatus[0].setText(label1);
         gxLaStatus[0].repaintNow();
      }
      if(label2!=null) {
         gxLaStatus[1].setText(label2);
         gxLaStatus[1].repaintNow();
      }
   }
}
