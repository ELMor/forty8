/** Join a set of given fonts into one archive
 * Copyright (c) 2002-2003 Guilherme Campos Hazan
 * Created for SuperWaba 3.0
 * This code is LGPL, as well is SuperWaba.
 * Must be compiled with JDK 1.2.2 or above.
 */
package sw.fontCreate;
import waba.io.Catalog;
import waba.io.DataStream;
import waba.io.ResizeStream;

public class JoinFonts {
   public JoinFonts(String fontName) {
      waba.applet.JavaBridge.showMsgs=false;
      waba.applet.JavaBridge.setNonGUIApp();
      Catalog out=new Catalog(fontName+".Font.Font", Catalog.READ_ONLY);
      if(out.isOpen()) {
         out.delete();
      }
      out=new Catalog(fontName+".Font.Font", Catalog.CREATE);
      ResizeStream rs=new ResizeStream(out, 16386);
      DataStream   ds=new DataStream(rs);
      if(!out.isOpen()) {
         fatalError("Can't create output file!");
      }
      String[] styles={"Small", "SmallBold", "Large", "LargeBold"};
      for(int i=0; i<styles.length; i++) {
         String  fileName=fontName+styles[i];
         Catalog in=new Catalog(fileName+".Font.Font", Catalog.READ_ONLY);
         if(!in.isOpen()) {
            System.out.println("Could not open catalog "+fileName);
            continue;
         }

         // read the record from the input catalog
         in.setRecordPos(0);
         int    size=in.getRecordSize();
         byte[] buf=new byte[size];
         in.readBytes(buf, 0, size);
         // write the record to the output catalog
         rs.startRecord();

         ds.writeCString(fileName);
         int l=fileName.length()+1;
         ds.pad(((int)((l+3)/4)*4)-l); // make sure it ends in a 4 byte boundary

         ds.writeBytes(buf, 0, size);
         rs.endRecord();
         in.close();
      }
      out.close();
      System.out.println("File written with success!");
   }

   private void fatalError(String msg) {
      System.out.println(msg);
      System.exit(1);
   }

   public static void main(String[] args) {
      if(args.length!=1) {
         System.out.println("Format: java JoinFonts <font name>");
         System.out.println("E.g.: java JoinFonts HSW");
         System.out.println("The small,smallbold,large,largebold styles are added to the package.");
      } else {
         new JoinFonts(args[0]);
      }
   }
}
