package sw.fontCreate;
import waba.fx.Rect;

/** Converts a Windows true type font to a pdb file that can be used by SW programs
 * (and also by other programs)
 * Copyright (c) 2002-2003 Guilherme Campos Hazan
 * Created for SuperWaba 3.0
 * This code is LGPL, as well is SuperWaba.
 * Must be compiled with JDK 1.2.2 or above.
 */
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.PixelGrabber;

public class TTF2PDBs {
   public static byte detailed=0; // 1 show messages, 2 show msgs + chars
   String             fontName;
   int                fontSize;
   PalmFont           pf;
   Component          comp;
   boolean            split;
   int[] bits={128, 64, 32, 16, 8, 4, 2, 1};

   public TTF2PDBs(
                   String fontName, String size, String arg3, String arg4,
                   String arg5
                  ) {
      boolean  split    =false;
      String   outName  =fontName; // guich@401_11
      String[] extraArgs=new String[]{arg3, arg4, arg5};
      for(int i=0; i<extraArgs.length; i++) {
         if(extraArgs[i]!=null) {
            String arg=extraArgs[i];
            if(arg.equalsIgnoreCase("/split")) {
               split=true;
            } else if(arg.toLowerCase().startsWith("/rename:")) {
               outName=arg.substring(8);
            } else if(arg.toLowerCase().startsWith("/detailed:")) {
               detailed=(byte)(arg.charAt(10)-'0');
               if((detailed!=1)&&(detailed!=2)) {
                  System.out.println("Invalid detailed value. Resetting to 0");
                  detailed=0;
               }
            }
         }
      }

      if(split) {
         System.out.println("Splitting files...");
      }
      waba.applet.JavaBridge.showMsgs=false;
      waba.applet.JavaBridge.setNonGUIApp();
      this.split   =split;
      // parse parameters
      this.fontName=fontName;
      try {
         fontSize=Integer.parseInt(size);
      } catch(NumberFormatException e) {
         println("Invalid font size: "+size);
         System.exit(2);
      }

      // create a valid component
      comp=new java.awt.Frame();
      comp.addNotify();
      // create fonts
      println("TTF2PDBs - Copyright (c) SuperWaba 2002-2003. Processing...");
      convertFont(
                  split ? (-1) : 0, new Font(fontName, Font.PLAIN, fontSize),
                  outName+"Small", split ? (outName+"Small") : outName
                 );
      convertFont(
                  split ? (-1) : 1, new Font(fontName, Font.BOLD, fontSize),
                  outName+"SmallBold", split ? (outName+"SmallBold") : outName
                 );
      convertFont(
                  split ? (-1) : 2, new Font(fontName, Font.PLAIN, fontSize+2),
                  outName+"Large", split ? (outName+"Large") : outName
                 );
      convertFont(
                  split ? (-1) : 3, new Font(fontName, Font.BOLD, fontSize+2),
                  outName+"LargeBold", split ? (outName+"LargeBold") : outName
                 );
      // test fonts
      runTestFont(outName);
   }

   public void println(String s) {
      System.out.println(s);
   }

   private void convertFont(int index, Font f, String fileName, String outName) {
      // check if we have the right font
      if(!f.getFontName().toLowerCase().startsWith(fontName.toLowerCase())) {
         System.out.println("Font "+fontName+" not found. Was replaced by "
                            +f.getFontName()
                           );
         System.exit(1);
      }

      FontMetrics fm    =comp.getFontMetrics(f);
      int         width =fm.charWidth('@')*6;
      int         height=fm.getHeight()*6;

      int         ini         =32;
      int         end         =255;
      int         invalidCharW=4;

      Image       img=comp.createImage(width, height);
      Graphics    g  =img.getGraphics();
      g.setFont(f);

      // Note: Sun's JDK does not return a optimal width value for the
      // characters, notabily chars W,V,T,A. Because of this, we need to
      // find the width ourselves.
      int[] widths   =new int[256];
      int[] gaps     =new int[256];
      short sum      =0;
      short totalBits=0;
      int   maxW     =0;
      int   maxH     =0;
      int   i;

      for(i=ini; i<=end; i++) {
         g.setColor(Color.white);
         g.fillRect(0, 0, width, height);
         g.setColor(Color.black);
         int x=width>>2;
         g.drawString(""+(char)i, x, height>>2);

         Rect r=computeBounds(getPixels(img, width, height), width);

         if(r==null) // blank char?
          {
            if(detailed==1) {
               println("char "+i+" is blank");
            }
            widths[i]=fm.charWidth(i);
            totalBits+=widths[i]; // guich@400_66
         } else {
            int w  =r.width+1; // +1 for interchar spacing
            int h  =r.height;
            int gap=x-r.x;
            maxW      =Math.max(maxW, w);
            maxH      =Math.max(maxH, h);
            gaps[i]   =gap; //<=0 ? 1 : 0; // most chars Java puts 1 pixel away; some chars Java puts one pixel near; for those chars, we make them one pixel right
            totalBits+=w;
            widths[i]=w;
            if(detailed==1) {
               println("Width of "+(char)i+" = "+widths[i]+" - gap: "+gap);
            }
         }
      }

      totalBits+=invalidCharW; // plus 4 for the invalid char

      maxH=fm.getHeight();
      // Create the PalmFont and set its parameters
      println("Original height: "+fm.getHeight()+", ascent: "+fm.getMaxAscent()
              +", descent: "+fm.getMaxDescent()+", leading: "+fm.getLeading()
             );
      pf               =new PalmFont(fontName, fileName, outName, index);
      pf.fontType      =0x9000;
      pf.firstChar     =ini;
      pf.lastChar      =end;
      pf.maxWidth      =maxW;
      pf.kernMax       =0;
      pf.nDescent      =0;
      pf.fRectWidth    =maxW;
      pf.fRectHeight   =fm.getHeight(); //(maxH+fm.getLeading());
      pf.descent       =fm.getMaxDescent();
      pf.ascent        =(maxH-pf.descent); //fm.getMaxAscent();
      pf.leading       =0; //fm.getLeading();
      pf.rowWords      =((totalBits+15)/16);
      pf.rowWords      =(((int)(pf.rowWords+1)/2)*2); // guich@400_67
      pf.owTLoc        =(
                           (pf.rowWords*pf.fRectHeight)
                           +(pf.lastChar-pf.firstChar)+8
                        );
      pf.debugParams();
      if(detailed==1) {
         println("totalBits: "+totalBits+", rowWords: "+pf.rowWords);
      }

      pf.initTables();

      // fill in PalmFont tables
      sum=0;
      for(i=ini; i<=end; i++) {
         pf.bitIndexTable[i-ini]=sum;
         sum+=widths[i];
      }
      pf.bitIndexTable[i-ini]=sum;
      i++; // invalid char position
      sum+=invalidCharW; // invalid char width
      pf.bitIndexTable[i-ini]   =sum;

      // draw the chars in the image and decode that image
      sum=0;
      for(i=ini; i<=end; i++) {
         int ww=widths[i];

         g.setColor(Color.white);
         g.fillRect(0, 0, width, height);
         g.setColor(Color.black);
         g.drawString(""+(char)i, gaps[i], pf.ascent);

         computeBits(getPixels(img, ww, maxH), sum, ww);
         sum+=ww;
      }

      // save the image
      pf.save();
   }

   private Rect computeBounds(int[] pixels, int w) {
      int white=-1;
      int x   =0;
      int y   =0;
      int xmin=10000;
      int ymin=10000;
      int xmax=-1;
      int ymax=-1;
      for(int i=0; i<pixels.length; i++) {
         if(pixels[i]!=white) {
            xmin   =Math.min(xmin, x);
            xmax   =Math.max(xmax, x);
            ymin   =Math.min(ymin, y);
            ymax   =Math.max(ymax, y);
         }

         if(++x==w) {
            x=0;
            y++;
         }
      }
      if(xmin==10000) { // empty char?
         return null;
      }

      return new Rect(xmin, ymin, xmax-xmin+1, ymax-ymin+1);
   }

   private void setBit(int x, int y) {
      pf.bitmapTable[(x/8)+(y*pf.rowWidthInBytes)]|=bits[x%8]; // set
   }

   private void computeBits(int[] pixels, int xx, int w) {
      int white=-1;
      int x=0;
      int y=0;
      for(int i=0; i<pixels.length; i++) {
         if(pixels[i]!=white) {
            setBit(xx+x, y);
            if(detailed==2) {
               System.out.print('@');
            }
         } else if(detailed==2) {
            System.out.print(' ');
         }

         if(++x==w) {
            x=0;
            y++;
            if(detailed==2) {
               System.out.println();
            }
         }
      }
   }

   public static int[] getPixels(Image img, int width, int height) {
      PixelGrabber pg=new PixelGrabber(img, 0, 0, width, height, true);
      try {
         pg.grabPixels();
      } catch(InterruptedException ie) {
      }
      return (int[])pg.getPixels();
   }

   public static void runTestFont(String fontName) {
      waba.applet.JavaBridge.showMsgs=true;
      waba.applet.JavaBridge.resetNonGUIApp();
      waba.applet.Applet.main(new String[]{
                                 "/useSonyFonts", "/w", "400", "/h", "400",
                                 "/scale", "1", "/cmdLine", fontName, "sw.fontCreate.TestFont"
                              }
                             );
   }

   public static void main(String[] args) {
      Font[] fonts=
                      GraphicsEnvironment.getLocalGraphicsEnvironment()
                                         .getAllFonts();
      if(args.length<2) {
         System.out.println("Format: java TTF2PDBs <font name> <size> </split> </rename:newName> </detailed:1_or_2>");
         System.out.println("One font file will be generated (only SuperWaba supports");
         System.out.println("this format. If /split is given (optional), four font files");
         System.out.println("will be generated (these fonts are recognized by all other softwares.");
         System.out.println("Use /rename:newName to rename the output font name.");
         System.out.println("Use /detailed:1_or_2 to show detailed information.");
         System.out.println();
         System.out.println("Alternative format: java TTF2PDBs test <font name>");
         System.out.println("This will open a SuperWaba app to test the font");
         System.out.println();
         System.out.println("Copyright (c) SuperWaba 2002-2003");
         System.out.println("Must use JDK 1.2.2 or higher!");
         System.out.println("\nAvailable fonts: <press enter to continue>");
         try {
            System.in.read();
         } catch(java.io.IOException ie) {
         }
         for(int i=0; i<fonts.length; i++) {
            System.out.println(" "+fonts[i].getName());
         }
      } else if(args[0].equalsIgnoreCase("test")) {
         runTestFont(args[1]);
      } else {
         new TTF2PDBs(
                      args[0], args[1], (args.length>2) ? args[2] : null,
                      (args.length>3) ? args[3] : null,
                      (args.length>4) ? args[4] : null
                     );
      }
   }
}
