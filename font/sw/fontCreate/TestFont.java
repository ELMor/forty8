package sw.fontCreate;
import waba.fx.*;
import waba.ui.*;
import waba.sys.*;

public class TestFont extends MainWindow
{
   private Button btn;

   public TestFont()
   {
      super("Test Font - SuperWaba", TAB_ONLY_BORDER);
   }
   public void onStart()
   {
      String fontName = getCommandLine();
      Vm.debug("font given: "+fontName);
      if (fontName == null || fontName.length() == 0)
         exit(1);
      Font fonts[] =
      {
         new Font(fontName, Font.PLAIN, 12), // small
         new Font(fontName, Font.PLAIN, 16), // large
			new Font(fontName, Font.BOLD,  12), // small bold
			new Font(fontName, Font.BOLD,  16), // large bold
      };
      for (int i = 0; i < 4; i++)
      {
         Label l = new Label(createLabel());
         l.setFont(fonts[i]);
         add(l,LEFT,AFTER+5);
      }
      add(btn = new Button("Exit"), CENTER,BOTTOM);
   }
   public void onEvent(Event e)
   {
      if (e.type == ControlEvent.PRESSED && e.target == btn)
         exit(0);
   }
   
   public String createLabel(){
   	String k="";
   	for(int i=0;i<16;i++){
   		for(int j=i*16;j<(i+1)*16;j++){
   			k+=Character.toString((char)j);
   		}
   		k+="|";
   	}
   	return k;
   }
}