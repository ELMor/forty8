package sw.fontCreate;
import waba.io.*;
public class PalmFont
{
   public int fontType;         // font type
   public int firstChar;        // ASCII code of first character
   public int lastChar;         // ASCII code of last character
   public int maxWidth;         // maximum character width
   public int kernMax;          // negative of maximum character kern
   public int nDescent;         // negative of descent
   public int fRectWidth;       // width of font rectangle
   public int fRectHeight;      // height of font rectangle
   public int owTLoc;           // offset to offset/width table
   public int ascent;           // ascent
   public int descent;          // descent
   public int leading;          // leading
   public int rowWords;         // row width of bit image / 2

   public int   rowWidthInBytes;
   public int   numberOfChars;
   public int   bitmapTableSize;
   public byte  []bitmapTable;
   public short []bitIndexTable;

   public String fileName; // SWSmall
   public String fontName; // SW
   public String outName; // SWSmall or SW or newName - guich@401_11
   public int index;

   static Catalog c;

   public void save()
   {
      if (index == -1 || index == 0)
      {
         // split mode
         c = new Catalog(outName+".Font.Font",Catalog.READ_ONLY);
         if (c.isOpen())
            c.delete();
         c = new Catalog(outName+".Font.Font",Catalog.CREATE);
      }
      if (c.isOpen())
      {
         ResizeStream rs = new ResizeStream(c,16386);
         DataStream ds = new DataStream(rs);
         rs.startRecord();
         if (index != -1)
         {
            ds.writeCString(fileName);
            int l = fileName.length()+1;
            ds.pad((int)((l+3) / 4)*4 - l); // make sure it ends in a 4 byte boundary
         }
         ds.writeShort(fontType   );
         ds.writeShort(firstChar  );
         ds.writeShort(lastChar   );
         ds.writeShort(maxWidth   );
         ds.writeShort(kernMax    );
         ds.writeShort(nDescent   );
         ds.writeShort(fRectWidth );
         ds.writeShort(fRectHeight);
         ds.writeShort(owTLoc     );
         ds.writeShort(ascent     );
         ds.writeShort(descent    );
         ds.writeShort(leading    );
         ds.writeShort(rowWords   );

         ds.writeBytes(bitmapTable);
         for (int i=0; i < bitIndexTable.length; i++)
            ds.writeShort(bitIndexTable[i]);
         rs.endRecord();
         if (index == -1 || index == 3) c.close();
         if (index == -1)
            System.out.println("File "+fileName+" saved.\n");
         else
            System.out.println("Font "+fileName+" stored in record "+index);
      }
   }
   public PalmFont(String fontName, String fileName, String outName, int index)
   {
      this.index = index;
      this.fontName = fontName;
      this.fileName = fileName;
      this.outName = outName;
   }

   public void debugParams()
   {
//      System.out.println("fontType   : "+fontType   );
      System.out.println("firstChar  : "+firstChar  );
      System.out.println("lastChar   : "+lastChar   );
      System.out.println("maxWidth   : "+maxWidth   );
      System.out.println("kernMax    : "+kernMax    );
      System.out.println("nDescent   : "+nDescent   );
      System.out.println("fRectWidth : "+fRectWidth );
      System.out.println("fRectHeight: "+fRectHeight);
      System.out.println("owTLoc     : "+owTLoc     );
      System.out.println("ascent     : "+ascent     );
      System.out.println("descent    : "+descent    );
      System.out.println("leading    : "+leading    );
      System.out.println("rowWords   : "+rowWords   );
   }

   public void initTables()
   {
      rowWidthInBytes = rowWords << 1;
      numberOfChars   = lastChar - firstChar+1;
      bitmapTableSize = (int)rowWidthInBytes * (int)fRectHeight;

      bitmapTable     = new byte[bitmapTableSize];
      bitIndexTable   = new short[numberOfChars+2];
   }
   public int charWidth(char ch)
   {
      int index = ch - firstChar;
      if (index < 0 || index > lastChar) index = numberOfChars; // MCS index
      // Get the source x coordinate and width of the character
      int offset = bitIndexTable[index];
      return (byte)(bitIndexTable[index+1] - offset);
   }
}
